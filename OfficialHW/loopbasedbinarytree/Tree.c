/*******************************
 
 Megan Chang
 CSE 12 2015
 cs12xee
 Nov 7 2015
 Assignment Seven

 Filename: Tree.c
 
 Description: This program implements a binary tree data structure with insert,
 lookup, and remove methods. This structure will be used to allow variable 
 assignment in the calculator.

 *******************************/

#include <cstdlib>
#include <string.h>
#include <cstdio>
#include <iostream>
#include "Tree.h"
using namespace std;

/* debug messages */
static const char ALLOCATE[] = " - Allocating]\n";
static const char AND[] = " and ";
static const char COMPARE[] = " - Comparing ";
static const char INSERT[] = " - Inserting ";
static const char TREE[] = "[Tree ";

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


/* struct TNode - Contains TNode's fields/members, each is declared and 
 * initialised. Contains TNode constructor, destructor, delete_allNodes method,
 * and methods to write to stream
 */

struct TNode {
	Base * data;
	TNode * left, * right, *parent;
	static long occupancy;
	unsigned long hasBeenDeleted;

	// left child's height - right child's height
	long balance;

	// 1 + height of tallest child, or 0 for leaf
	long height;


	/*
	 * Function:		TNode
	 * Purpose:			TNode constructor
	 * Description:		Allocates and initializes the memory associated with
	 *					the TNode object. Each data member is initialised.
	 *					Occupancy is incremented.
	 * Input:			element = data stored in the node
	 * Result:			TNode object created
	 */

	TNode (Base * element) : data (element), left (0), right (0),
	parent (0), hasBeenDeleted (FALSE), balance (0), height(0) {
		occupancy++;
	}
	
	/*
	 * Function:		~Tnode
	 * Purpose:			TNode desctructor
	 * Description:		Called when deallocating this TNode. Sets fields 
	 *					to null, deletes data, decrements occuapncy. 
	 * Input:			nothing
	 * Result:			Tnode object "deleted" (lazy delete for hw7)
	 */
	
	~TNode (void) {
		delete data;
		data = NULL;
		left = NULL;
		right = NULL;
		parent = NULL;
		occupancy--;
		hasBeenDeleted = 1;
	}
	ostream & Write (ostream & stream) const {
		stream << "at height:  " << height << "  with balance:  "
			<< balance << "  ";
		return data->Write (stream) << "\n";
	}
	ostream & Write_AllTNodes (ostream & stream) const {
		if (left)
			left->Write_AllTNodes (stream);
		/* visit - move these two lines for diff traversal */
		if (!hasBeenDeleted)
			Write (stream);
		if (right)
			right->Write_AllTNodes (stream);

		return stream;
	}

	/*
	 * Function:		delete_AllTNodes
	 * Purpose:			Deletes all TNodes at once
	 * Description:		Goes through TNodes in post-fix traversal order, and
	 *					calls TNode destructor
	 * Input:			nothing
	 * Result:			All TNodes are deleted
	 */

	void delete_AllTNodes (void) {
		/* creates TNode pointer variable, sets it to calling object (root) */
		TNode * working = this;
		/* creates TNode pointer variable to store value to be deleted */
		TNode * to_delete;

		/* while root node exists */
		while (working != NULL){
			/* if there is a left child */
			if (working->left != NULL){
				/* set working to left child */
				working = working->left;
				/* set parent's left pointer to null */
				working->parent->left = NULL;
			}
			/* if there is a right child */
			else if (working->right != NULL) {
				/* set working to right child */
				working = working->right;
				/* set parent's right pointer to null */
				working->parent->right = NULL;
			}
			/* if no child nodes */
			else {
				/* set node to be deleted to working */
				to_delete = working;
				/* set working to node-to-be-deleted's parent */
				working = working->parent;
				/* delete working */
				delete to_delete;
				/* if working node is root note, delete it */
			}
		}
	}
};

// initialization of static data fields of Tree
long TNode :: occupancy = 0;
bool Tree :: debug_on = 0;

void Tree :: Set_Debug (bool option) {
	debug_on = option;
}

/*
 * Function:		Tree
 * Purpose:			Tree constructor
 * Description:		Allocates and initializes the memory associated with the
 *					Tree object, initalised data fields.
 * Input:			name - String displayed when writing the Tree
 * Result:			Tree object constructed
 */

Tree :: Tree (const char * name) : root (0), tree_name (strdup (name))
 /***************************************************************************
	% Routine Name : Tree :: Tree  (public)
	% File :         Tree.c
	% 
	% Description :  Initializes root pointer to NULL, and occupancy to 0.
 ***************************************************************************/
{
	if (debug_on)
		cerr << TREE << tree_name << ALLOCATE;
}

/*
 * Function:		~Tree
 * Purpose:			Tree destructor
 * Description:		Called when de-allocating this Tree. Deallocates all
 *					in the tree.
 * Input:			nothing
 * Result:			Tree is deleted
 */

Tree :: ~Tree (void)
	/***************************************************************************
	  % Routine Name : Tree :: ~Tree  (public)
	  % File :         Tree.c
	  % 
	  % Description :  deallocates memory associated with the Tree.  It
	  %                will also delete all the memory of the elements within
	  %                the table.
	 **************************************************************************/
{
	/* sets tree to null, frees tree_name memory*/
	free ((void *)(tree_name));
	tree_name = NULL;
	/* calls delete_AllNodes on root node */
	root->delete_AllTNodes();
}       /* end: ~Tree */

/*
 * Function:		Insert
 * Purpose:			Inserts element into binary tree
 * Description:		If there isn't a root node, insert one. If there is,
 *					if the new data is the same as data already in tree, 
 *					override old data with new data. Compare element of node
 *					to be inserted with nodes already in tree and insert to 
 *					left or right of node in tree based on alphabetical order.
 *					Update height and balance of each after every insertion.
 * Input:			element - complete elements (name and number) to insert
 * Result:			Node is inserted into tree
 */

unsigned long Tree :: Insert (Base * element) {

	/* creates TNode pointer variable and sets it to root, this will be 
	 * "current" node being compared to or inserted */
	TNode * working = root;

	/* boolean to check if node has been inserted, for updating height/balance*/
	bool inserted = false;

	/* height of left child node, initialised to 0 */
	long leftHeight = 0;

	/* height of right child node, initialised to 0 */
	long rightHeight = 0;

	/* largest height between left/right child node, used to set height of 
	 * parent node */
	long maxHeight = 0;

	/* if root doesn't exist, ie. nothing in tree */
	if (root == NULL){

		/* make new (and 1st ) node, inserts into tree */
		root = new TNode(element);
		if (debug_on){
			cerr << TREE << tree_name << INSERT << (const char*)(*element) << 
			"]" << "\n";
		}

		/* exits fxn, node inserted succesfully */
		return true;
	}
	
	/* while new node hasn't been inserted */ 
	else while (!inserted){

		if (debug_on){
			cerr << TREE << tree_name << COMPARE << (const char*)(*element) << 
				AND << *working->data << "]" << "\n";
		}

		/* duplicate insert case */
		if (*element == *working->data){

			/* delete data in node already in tree */
			delete working->data;
			/* set data of that node to element to be inserted */
			working->data = element;
			/* set inserted boolean to true */
			inserted = true;
			/* correct boolean flag so inserted element will be printed */
			working->hasBeenDeleted = false;
			if (debug_on)
				cerr << TREE << tree_name << INSERT << (const char*)(*element) <<
				"]" << "\n";
		}
		
		/* insert to left of working node */
		else if (*element < *working->data){
			/* if working has no left child */
			if (working->left == NULL){
				/* create/insert new node */
				working->left = new TNode(element);
				/* attach new node to current node */
				working->left->parent = working;
				/* set inserted boolean to true */
				inserted = true;
				if (debug_on)
					cerr << TREE << tree_name << INSERT << (const char*)(*element)
					<< "]" << "\n";
			}
			/* if working already has left child, move down tree, set working
			 * to left child*/
			else {
				working = working->left;
			}
		}
		
		/* insert to right of working node */
		else {
			/* if working has no right child */
			if (working->right == NULL){
				/* create/insert new node */
				working->right = new TNode(element);
				/* attach new node to current node */
				working->right->parent = working;
				/* set inserted boolean to true */
				inserted = true;
				if (debug_on)
					cerr << TREE << tree_name << INSERT << 
					(const char*)(*element) << "]" << "\n";
			}
			/* if working already has right child, move downt tree, set working
			 * to right child*/
			else {
				working = working->right;
			}
		}
	}
	
	/* to update height and balance after new node inserted */
	while (inserted && working != NULL){
		/* if working has no left child, set leftHeight var to -1, otherwise
		 * set it to height of left child*/
		if (working->left == NULL){
			leftHeight = -1;
		}
		else {
			leftHeight = working->left->height;
		}
		/* if working has no right child, set rightHeight var to -1, otherwise
		 * set it to height of right child */
		if (working->right == NULL){
			rightHeight = -1;
		}
		else {
			rightHeight = working->right->height;
		}
		/* compare left/rightHeight var to find max height to use in balance
		 * calculation, assign it to maxHeight */
		if (leftHeight > rightHeight) {
			maxHeight = leftHeight;
		}
		else {
			maxHeight = rightHeight;
		}
		/* set working's height */
		working->height = 1 + maxHeight;
		/* sets working's balance */
		working->balance = leftHeight - rightHeight;
		/* sets working to inserted node's parent */
		working = working->parent;
	}
	return true;
}


/* 
 * Function:		Lookup
 * Purpose:			Searches for particular element in binary tree.
 * Description:		Goes through each node in tree, if name looking for is 
 *					found, return pointer to what is found, else return null.
 * Input:			element - incomplete element (name, no number)
 * Result:			const Base pointer to element returned, or null if elmeent
 *					isn't found.
 */

const Base * Tree :: Lookup (const Base * element) const {
	
	/* creates pointer to TNode object, sets it to root */
	TNode * working = root;
	
	/* if tree is empty, print debug statmenet */
	if (root == NULL){
		if (debug_on)
			cerr << "locating from empty tree";
	}

	/* while tree is not empty */
	while (working != NULL){
		if (debug_on)
			cerr << TREE << tree_name << COMPARE << (const char*)(*element) << 
			AND << *working->data << "]" << "\n";
		/* compare name looking for to working node data */
		if (*element == *working->data){
			/* and if node that name is found in has not been "deleted", return
			 * the data, else return null */
			if (working->hasBeenDeleted){
				return NULL;
			}
			else {
				return working->data;
			}
		}
		/* if name looking for is less than or greater than name of current data
		 * being compared, compare with data of working nodes' left child or
		 * right child, respectively */
		else if (*element < *working->data) {
			working = working->left;
		}
		else {
			working = working->right;
		}
	}
	
	/* if name looking for is not in tree, return null */
	return NULL;
}

/* 
 * Function:		Remove
 * Purpose:			removes matching data from tree
 * Description:		Looks for element to remove in tree, moving to left/right 
 *					child as appropriate, then set boolean flag to false,
 *					effectively "deleting" it by pretending it's not there
 * Input:			element - incomplete elements (name, no number)
 * Result:			returns pointer to data if element to be removed is found, 
 *					otherwise returns null
 */

Base * Tree :: Remove (const Base * element) {
	/* creates/initialises TNode pointer variable working to root */	
	TNode * working = root;
	
	/* if tree is empty, print debug statmenet */
	if (root == NULL){
		if (debug_on)
			cerr << "removing from empty tree";
		// exit fxn
		return NULL;
	}

	/* while tree is not empty */
	while (working != NULL){
		if (debug_on)
			cerr << TREE << tree_name << COMPARE << (const char*)(*element) <<
			AND << *working->data << "]" << "\n";
		/* if name is found to match data in current node being compared to, and
		 * it hasn't yet been deleted, delete it by setting flag to true, then
		 * decrement occupancy and return data of node "removed" */
		if (*element == *working->data){
			if (working->hasBeenDeleted){
				return NULL;
			}
			else {
				working->hasBeenDeleted = true;
				TNode :: occupancy--;
				return working->data;
			}
		}
		/* if name is less than or greater than name in node being compare to, 
		 * compare name to left and right child of current node, respectively */
		else if (*element < *working->data) {
			working = working->left;
		}
		else {
			working = working->right;
		}
	}

	/* return null if name not found in tree and remove unsuccessful */
	return NULL;
}

ostream & Tree :: Write (ostream & stream) const
/***************************************************************************
  % Routine Name : Tree :: Write (public)
  % File :         Tree.c
  % 
  % Description : This function will output the contents of the Tree table
  %               to the stream specificed by the caller.  The stream could be
  %               cerr, cout, or any other valid stream.
  %
  % Parameters descriptions :
  % 
  % name               description
  % ------------------ ------------------------------------------------------
  % stream             A reference to the output stream.
  % <return>           A reference to the output stream.
 ***************************************************************************/
{
	stream << "Tree " << tree_name << ":\n"
		<< "occupancy is " << TNode :: occupancy << " elements.\n";

	return (root) ? root->Write_AllTNodes (stream) : stream;
}       /* end: Write */

