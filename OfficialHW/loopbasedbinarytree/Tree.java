/**
 * Megan Chang
 * CSE 12 2015
 * cs12xee
 * Nov 7 2015
 * Assignment Seven
 *
 * Filename: Tree.java
 *
 * Description: This program implements a binary tree data
 * structure with insert, lookup, and remove methods; this 
 * structure will be used to allow variable
 * assignment in the calculator.
 */



public class Tree<Whatever extends Base> extends Base {

	/* data fields */
	private TNode root;			// root node in tree
	private long occupancy;		// number of nodes in tree 
	private String treeName;	// name of tree

	/* debug flag */
	private static boolean debug;

	/* debug messages */
	private static final String ALLOCATE = " - Allocating]\n";
	private static final String AND = " and ";
	private static final String COMPARE = " - Comparing ";
	private static final String INSERT = " - Inserting ";
	private static final String TREE = "[Tree ";

	
	/*
	 * Function:		Tree (constructor)
	 * Purpose:			Constructs binary tree, allocates/initialises memory 
	 *					associated with tree
	 * Description:		Initialises data fields
	 * Input:			name - string displayed when writing tree
	 * Result:			Binary tree created
	 */

	public Tree (String name) {
		treeName = name;
		occupancy = 0;
		root = null;
		if (debug)
			System.err.print(TREE + name + ALLOCATE);
	}

	/*
	 * Function:		debugOff
	 * Purpose:			turns off debugging for this tree
	 * Description:		sets debug status to false
	 * Input:			nothing
	 * Result:			debug turned off
	 */

	public static void debugOff () {
		debug = false;
	}

	/*
	 * Function:		debugOn
	 * Purpose:			turns on debugging for this tree
	 * Description:		sets debug status to true
	 * Input:			nothing
	 * Result:			debug turned off
	 */

	public static void debugOn () {
		debug = true;
	}

	/**
	 * Returns the tree's name
	 * @return name of the tree
	 */
	public String getName() {
		return treeName;
	}
		
	/*
	 * Function:		insert
	 * Purpose:			Inserts element into binary tree
	 * Description:		If there isn't a root node, insert one. If there is,
	 *					if the new data is the same as data already in tree,
	 *					override old data with new data. Compare element of node
	 *					to be inserted with nodes already in tree and insert to
	 *					left or right of node in tree based on alphabetical 
	 *					order. Update height and balance of each after every 
	 *					insertion.
	 * Input:			element - complete elements (name/number) to insert
	 * Result:			Node is inserted into tree, return true for succesful
	 *					insert, false if not
	 */

	public boolean insert (Whatever element) {

		// creates Tnode pointer variable and sets it to root, this will be 
		// "current node being compared to or inserted 
		TNode working = root;
		// boolean to check if node has been inserted, to udpate height/balance
		boolean inserted = false;
		// height of left child node, initialised to 0
		long leftHeight = 0;
		// height of right child node, initialised to 0
		long rightHeight = 0;
		// largest height between left/right child node, used to set height of 
		// parent node
		long maxHeight = 0;

		// to insert first node into tree 
		if (root == null){
			// make new node, insert into tree
			root = new TNode(element);
			if (debug)
				System.err.println(TREE + treeName + INSERT + element.getName() 
						+ "]");
			// exist fxn, node inserted succesfully
			return true;
		}

		// while new node hasn't been inserted
		else while (!inserted){

			if (debug){
				System.err.println(TREE + treeName + COMPARE + element.getName() 
						+ AND + working.data.getName() + "]");
			}

			// duplicate insert case
			if (element.equals(working.data)){

				// override old element with new element 
				working.data = element;
				// set inserted boolean to true
				inserted = true;
				// correct boolean flat so inserted element will be printed
				working.hasBeenDeleted = false;
				if (debug)
					System.err.println(TREE + treeName + INSERT + 
							element.getName() + "]");
			}

			// insert to left of working node
			else if (element.isLessThan(working.data)){
				// if working has no left chlid
				if (working.left == null){
					// create/insert new node
					working.left = new TNode(element);
					// attach new node to current node
					working.left.parent = working;
					// set inserted boolean to true
					inserted = true;
					if (debug)
						System.err.println(TREE + treeName + INSERT + 
								element.getName() + "]");
				}
				// if working already has left child, move down tree, set 
				// working to left child
				else {
					working = working.left;
				}
			}

			// insert to right of working node
			else {
				// if working has no right child
				if (working.right == null){
					// create/insert new node
					working.right = new TNode(element);
					// attach new node to current node
					working.right.parent = working;
					// set inserted boolean to true
					inserted = true;
					if (debug)
						System.err.println(TREE + treeName + INSERT + 
								element.getName() + "]");
				}
				// if working already has right child, move down tree, set
				// working to right child
				else {
					working = working.right;
				}
			}
		}
		
		// to update height and balance after new node inserted
		while (inserted && working != null){
			// if working has no left child, set leftHeight var to -1, otherwise
			// set it to height of left child
			if (working.left == null){
				leftHeight = -1;
			}
			else {
				leftHeight = working.left.height;
			}
			// if working has no right child, set rightHeight var to -1, 
			// otherwise set it to height of right child
			if (working.right == null) {
				rightHeight = -1;
			}
			else {
				rightHeight = working.right.height;
			}
			// compare left/rightHeight var to find max height to use in balance
			// calculations, assign it to maxHeight
			if (leftHeight > rightHeight){
				maxHeight = leftHeight;
			}
			else{
				maxHeight = rightHeight;
			}
			// set working's height
			working.height = 1 + maxHeight;
			// sets working's balance
			working.balance = leftHeight - rightHeight;
			// sets working to inserted node's parent
			working = working.parent;
		}
		// exits fxn, insert succesful
		return true;
	}

	/*
	 * Function:		lookup
	 * Purpose:			Searches for particular element in binary tree
	 * Description:		Goes through each node in tree, if name looking for is
	 *					found, return what is found, otherwise return null
	 * Input:			element = incomplete element (name, no number)
	 * Result:			returns element if found, null if not
	 */

	public Whatever lookup (Whatever element) {
		// creates pointer to TNode object, sets it to root
		TNode working = root;
		// if tree is empty, print debug statement
		if (root == null) {
			if (debug)
				System.err.println("locating from empty tree");
		}
		// while tree is not empty
		while (working != null){
			if (debug)
				System.err.println(TREE + treeName + COMPARE + element.getName()
				+ AND + working.data.getName() + "]");
			// compare name looking for to working node ata
			if (element.equals(working.data)){
				// and if node that name is found in has not been "deleted", 
				// return the data, else return null
				if (working.hasBeenDeleted){
					return null;
				}
				else {
					return working.data;
				}
			}
			// if name looking for is less than or greater than name of current
			// data being compared, compare with data of working node's left 
			// or right child, respectively
			else if (element.isLessThan(working.data)){
				working = working.left;
			}
			else {
				working = working.right;
			}
		}
		// if name looking for is not in tree, return null
		return null;
	}
	
	/*
	 * Function:		remove
	 * Purpose:			removes matching data from tree
	 * Descriptoin:		look for element to remove in tree, moving to left/right
	 *					child as appropriate, then set boolean flag to false,
	 *					effectively "deleting" it by pretending it's not there.
	 * Input:			element- incomplete elements (name, no number)
	 * Result:			returns data if element to be removed is 
	 *					found, otherwise return null
	 */
	
	public Whatever remove (Whatever element) {
		// creates/initialises TNode pointer variable working to root
		TNode working = root;
		// if tree is empty, print debug statmenet
		if (root == null){
			if (debug)
				System.err.println("removing from empty tree");
			// exit fxn
			return null;
		}
		// while tree is not empty
		while (working != null){
			if (debug)
				System.err.println(TREE + treeName + COMPARE + element.getName() 
				+ AND + working.data.getName() + "]");
			// if name is found to match data in current node being compared to,
			// and it hasnt yet been deleted, delete it by setting flag to true,
			// then decrement occupancy and return data of node "removed"
			if (element.equals(working.data)){
				if (working.hasBeenDeleted){
					return null;
				}
				else {
					working.hasBeenDeleted = true;
					occupancy--;
					return working.data;
				}
			}
			// if name is less than or greater than name in node being compared
			// to, compare name to left and right child of current node,
			// respectively
			else if (element.isLessThan(working.data)){
				working = working.left;
			}
			else {
				working = working.right;
			}
		}
		// return null if name not found in tree and remove unsuccessful
		return null;
	}

	/**
	 * Creates a string representation of this tree. This method first
	 * adds the general information of this tree, then calls the
	 * recursive TNode function to add all nodes to the return string 
	 *
	 * @return  String representation of this tree 
	 */
	public String toString () {
		String string = "Tree " + treeName + ":\noccupancy is ";
		string += occupancy + " elements.";

		if(root != null)
			string += root.writeAllTNodes();

		return string;
	}

	/**
	 * Private class Tnode - Contains data TNode's fields/members, each 
	 * is declared and initialised; contains TNode, constructor and
	 * methods to write to stream 
	 */
	
	private class TNode {

		public Whatever data;			// data in each node
		public TNode left, right, parent; // left, right, parent of node
		public boolean hasBeenDeleted; // boolean flag to mark deleted node

		/* left child's height - right child's height */
		public long balance;
		/* 1 + height of tallest child, or 0 for leaf */
		public long height;

		/*
		 * Function:		TNode (constructor)
		 * Purpose:			Allocates and initialises memory associated with
		 *					Tnode objet. Each data member is initialised. 
		 *					Occupancy is incremnted.
		 * Input:			element - data stored in node
		 * Result:			Tnode object created
		 */

		public TNode (Whatever element) {
			occupancy++;
			data = element;
			left = null;
			right = null;
			parent = null;
			hasBeenDeleted = false;
			balance = 0;
			height = 0;
		}

		/**
		 * Creates a string representation of this node. Information
		 * to be printed includes this node's height, its balance,
		 * and the data its storing.
		 *
		 * @return  String representation of this node 
		 */

		public String toString () {
			return "at height:  " + height + "  with balance: " +
				balance + "  " + data;
		}

		/**
		 * Writes all TNodes to the String representation field. 
		 * This recursive method performs an in-order
		 * traversal of the entire tree to print all nodes in
		 * sorted order, as determined by the keys stored in each
		 * node. To print itself, the current node will append to
		 * tree's String field.
		 */
		public String writeAllTNodes () {
			String string = "";
			if (left != null)
				string += left.writeAllTNodes ();
			if (!hasBeenDeleted) 
				string += "\n" + this;          
			if (right != null)
				string += right.writeAllTNodes ();

			return string;
		}
	}
}
