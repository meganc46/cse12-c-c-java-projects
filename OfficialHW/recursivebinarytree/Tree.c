
/******
 *
 * Megan Chang
 * CSE 12 Fal 2015
 * cs12xee 
 * HW 8
 * Nov 23 2015
 *
 * Filename: Tree.c
 * Descriptoin: implements binary tree as in hw7, but recursively, and with
 * a proper remove, as well as threshold check
 *
 *
 ******/

#include <cstdlib>
#include <string>
#include "Tree.h"
using namespace std;

// messages
static const char AND[] = " and ";
static const char COMPARE[] = " - Comparing ";
static const char DEALLOCATE[] = " - Deallocating]\n";
static const char INSERT[] = " - Inserting ";
static const char REPLACE[] = " - Replacing ";
static const char UPDATE[] = " - Updating ";

template <class Whatever>
int Tree<Whatever>::debug = 0;

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define THRESHOLD 2

template <class Whatever>
ostream & operator << (ostream &, const TNode<Whatever> &);

template <class Whatever>
struct TNode {
        long balance;
        Whatever data;
        long height;
        TNode<Whatever> * left;
        long & occupancy;
        TNode<Whatever> * right;
        unsigned long & tree_count;
       

		/* TNode Constructor - initialises variables/fields, increments
		 * occupancy 
		 */
        TNode (const Whatever & element, Tree<Whatever> & theTree)
                : balance (0), data (element), height (0), left (0), 
                occupancy (theTree.occupancy), right (0), 
                tree_count (theTree.tree_count) {
			occupancy++;
		}
        
		/* TNode Constructor - initialises variables/fields, increments
		 * occupancy 
		 */
        TNode (const Whatever & element, TNode<Whatever> & parentTNode)
        : balance (0), data (element), height (0), left (0), 
                occupancy (parentTNode.occupancy), right (0), 
                tree_count (parentTNode.tree_count) {
			occupancy++;
		}
		
		/* Tnode Destructor - initialises variables/fields, decrements
		 * occupancy 
		 */
        ~TNode (void) {
			occupancy--;
		
		}

        void delete_AllTNodes (void) {
			if (this == NULL)
				return;
			if (left)
				left->delete_AllTNodes();
			if (right)
				right->delete_AllTNodes();
			delete(this);
		}

        unsigned long Insert (const Whatever & element, 
                              TNode<Whatever> *& PointerInParent);
		
		// OPTIONAL TNode :: Lookup => uncomment if implementing recursively
        // unsigned long Lookup(Whatever & element) const;
		
		unsigned long Lookup(Whatever & element) const {
			long success = 0;
			
			if (Tree<Whatever>::debug){
				cerr << TREE << tree_count << COMPARE << (const char *) element
				<< AND << (const char *) data << "]\n";
			}
			if (element == data){
				element = data;
				return 1;
			}
			else if (element < data){
				if (!left) 
					return 0;
				else
					success = left->Lookup(element);
			}
			else {
				if (!right)
					return 0;
				else 
					success = right->Lookup(element);
			}
			return success;
		}

        void ReplaceAndRemoveMin (TNode<Whatever> & targetTNode, 
                TNode<Whatever> *& PointerInParent){
			if (Tree<Whatever>::debug){
				cerr << TREE<< tree_count << COMPARE << (const char *) data 
				<< "]\n";
			}
			if (left == NULL){
				if (Tree<Whatever>::debug){
					cerr << TREE << tree_count << REPLACE << (const char *) data
					<< "]\n";
				}
				targetTNode.data = data;
				PointerInParent = right;
				delete(this);
			}
			else {
				left->ReplaceAndRemoveMin(targetTNode, left);
			}
			if (PointerInParent){
				PointerInParent->SetHeightAndBalance(PointerInParent);
			}
		}

        unsigned long Remove (TNode<Whatever> & elementTNode, 
                TNode<Whatever> *& PointerInParent,
                long fromSHB = FALSE){
			long SHAB = 1;
			if (Tree<Whatever>::debug){
				cerr << TREE << tree_count << COMPARE << (const char *)
				elementTNode.data << AND << (const char *) data << "]\n";
			}
			if (elementTNode.data == data){
				elementTNode.data = data;
				if (!right && !left){
					PointerInParent = NULL;
					delete(this);
				}
				else if (right && left){
					if (fromSHB){
						SHAB = 0;
						Whatever saved = elementTNode.data;
						right->ReplaceAndRemoveMin(*this, PointerInParent->right);
						Insert(saved, PointerInParent);
					}
					else {
						SHAB = 0;
						right->ReplaceAndRemoveMin(*this, PointerInParent->right);
						if (PointerInParent)
							SetHeightAndBalance(PointerInParent);
					}
				}
				else {
					if (right)
						PointerInParent = right;
					else
						PointerInParent = left;
					if (fromSHB)
						PointerInParent->Insert(elementTNode.data, PointerInParent);
					delete(this);
				}
				return 1;
			}
			else if (elementTNode.data < (data)) {
				if (!left)
					return 0;
					SHAB = left->Remove(elementTNode, left);
			}
			else {
				if (!right)
					return 0;
				SHAB = right->Remove(elementTNode, right);
			}
			if (SHAB)
				SetHeightAndBalance(PointerInParent);
			return SHAB;
		}		

        void SetHeightAndBalance (TNode<Whatever> *& PointerInParent) {
			long leftHeight = -1;
			long rightHeight = -1;

			if (left)
				leftHeight = left->height;
			if (right)
				rightHeight = right->height;
			if (leftHeight > rightHeight)
				height = leftHeight + 1;
			else
				height = rightHeight + 1;
			
			balance = leftHeight - rightHeight;
			
			if (Tree<Whatever>::debug){
				cerr << TREE << tree_count << UPDATE << (const char *) data
				<< "]\n";
			}
			if (abs(balance) > THRESHOLD)
				Remove(*this, PointerInParent, TRUE);
		}

        ostream & Write_AllTNodes (ostream & stream) const {
                if (left)
                        left->Write_AllTNodes (stream);
                stream << *this;
                if (right)
                        right->Write_AllTNodes (stream);

                return stream;
        }
};

template <class Whatever>
void Tree<Whatever> :: Set_Debug_On(){
	Tree<Whatever>::debug = 1;
}

template <class Whatever>
void Tree<Whatever> :: Set_Debug_Off(){
	Tree<Whatever>::debug = 0;
}


/* File:	Insert
 * Purpose:	To insert individual nodes into tree recursively
 * Description: checks for duplicate insert, and left and right child
 * nodes, calls insert if if those nodes aren't empty 
 * Input:	Element to be inserted, and pointerinparent
 * Result:  returns 1 for succesfful and 0 otherwise
 */

template <class Whatever>
unsigned long TNode<Whatever> :: Insert (const Whatever & element, 
                                         TNode<Whatever> *& PointerInParent) {
	
	if (Tree<Whatever>::debug){
		cerr << TREE<< tree_count << COMPARE << (const char *) element << AND 
		<< (const char *) data << "]\n";
	}
	if (element == data){
		if (Tree<Whatever>::debug){
			cerr << TREE << tree_count << INSERT << (const char *) element 
			<< "]\n";
		}
		data = element;
	}
	else if (element < data){
		if (!left){
			left = new TNode<Whatever>(element, *this);
			if (Tree<Whatever>::debug){
				cerr << TREE << tree_count << INSERT << (const char *) element
				<< "[\n";
			}
		}
		else
			left->Insert(element, left);
	}
	else {
		if (!right){
			right = new TNode<Whatever>(element, *this);
			if (Tree<Whatever>::debug){
				cerr << TREE << tree_count << INSERT << (const char *) element
				<< "[\n";
			}
		}
		else 
			right->Insert(element, right);
	}
	SetHeightAndBalance(PointerInParent);
	return 1;
}

/*
	if (*element == *this->data) {
		delete this->data;
		this->data = element;
	}
	else if (*element < *this->data){
		if (!left)
			left = new TNode<Whatever>(element, *this);
		else
			left->Insert(element, left);
	}
	else {
		if (!right)
			right = new TNode<Whatever>(element, *this);
		else
			right->Insert(element, right);
	}	
	return 1;
}
*/

template <class Whatever>
ostream & operator << (ostream & stream, const TNode<Whatever> & nnn) {
        stream << "at height:  :" << nnn.height << " with balance:  "
                << nnn.balance << "  ";
        return stream << nnn.data << "\n";
}

/* File:	insert
 * Purpose:	Tree's insert, calls Tnode's insert to insert node into tree
 * Descritoin: if root node exists, call insert again, if not, create new
 * node
 * Input:	element ot be inserted
 * Result:	returns 1 for successful insert, 0 otherwise
 */
template <class Whatever>
unsigned long Tree<Whatever> :: Insert (const Whatever & element) {
	if (occupancy == 0){
		root = new TNode<Whatever>(element, *this);
		if (Tree<Whatever>::debug){
			cerr << TREE << tree_count << INSERT << (const char *) element 
			<< "]\n";
		}
	}
	else
		root->Insert(element, root);
	return 1;
}

/*
	if (root == NULL){
		root = new TNode<Whatever>(element, *this);
	}
	else{
		root->Insert(element, root);
	}
	return 0;
}
*/

/* File:	Lookup
 * Purpose:	looks for element in tree
 * Description:	looks through left and right child nodes, if not found,
 * keep going, if found, return 1
 * Input:	element searching for
 * Result: return 1 for element found and 0 otherwise
 */
template <class Whatever>
unsigned long Tree<Whatever> :: Lookup (Whatever & element) const {
	if (!root)
		return 0;
	return root->Lookup(element);
}
	/*
	TNode<Whatever> * working = root;

	if (!root)
		return 0;
	
	while (!working){
		if (*element == *working->data){
			return 1;
		}
		else if (*element < *working->data) {
			working = working->left;
		}
		else {
			working = working->right;
		}
	}	
	*/
	/* recursive */
	/*
	unsigned long status;

	if (!root){
		status = 0;
		return status;
	}
	else {
		if (*element == *root->data)
			return 1;
		else if (*element < *root->data) {
			if (!left)
				return 0;
			else 
				status = this->left->Lookup(element);
		}
		else {
			if (!right) 
				return 0;
			else 
				status = this->right->Lookup(element);
		}
	}
	return status;
	*/

/* File: Remove
 * Purpose: to recursively remove nodes form tree
 * Description:
 *	calls lookup to traverse through tree, stopping when end of branch or
 *	item is found. when item is found, if node is leaf node, delete it, if
 *	node is tnode with 1 child, reassign its left and right node and delete
 *	it, if node is tnode with 2 children, find predecessor to replace it
 *	with the current tnode
 * Input: element to be removed
 * Result: returns 1 for succesful and 0 otherwise
 */

template <class Whatever>
unsigned long Tree<Whatever> :: Remove (Whatever & element) {
	long removed;
	if (occupancy == 0)
		return 0;
	TNode<Whatever> toFind(element, *this);
	removed = root->Remove(toFind, root, FALSE);
	element = toFind.data;
	return removed;
}
                                
/* Tree Destructor - deletes all nodes and frees memory */
template <class Whatever>
Tree<Whatever> :: ~Tree (void)
/***************************************************************************
% Routine Name : Tree<Whatever> :: ~Tree  (public)
% File :         Tree.c
% 
% Description :  deallocates memory associated with the Tree.  It
%                will also delete all the memory of the elements within
%                the table.
***************************************************************************/
{
	if (debug)
		cerr << TREE << tree_count << DEALLOCATE;
	if (!root) {}
	else 
		root->delete_AllTNodes();
	tree_count--;
}



template <class Whatever>
ostream & Tree<Whatever> :: Write (ostream & stream) const
/***************************************************************************
% Routine Name : Tree<Whatever> :: Write (public)
% File :         Tree.c
% 
% Description : This function will output the contents of the Tree table
%               to the stream specificed by the caller.  The stream could be
%               cerr, cout, or any other valid stream.
%
% Parameters descriptions :
% 
% name               description
% ------------------ ------------------------------------------------------
% stream             A reference to the output stream.
% <return>           A reference to the output stream.
***************************************************************************/
{
        stream << "Tree " << tree_count << ":\n"
                << "occupancy is " << occupancy << " elements.\n";

        return (root) ? root->Write_AllTNodes (stream) : stream;
}
