/***
 *
 * Megan Chang
 * CSE 12 Fall 2015
 * cs12xee
 * HW8
 * Nov 23 2015
 *
 * File: Driver.h
 * Description: Header file, contains method decalrations and
 * private/publici nfo
 */

#ifndef DRIVER_H
#define DRIVER_H

#include <string.h>
#include <iostream>
#include <cstdlib>
using namespace std;

/* length of the character name array */
const int length = 20; 

class UCSDStudent {
        friend ostream & operator << (ostream &, const UCSDStudent &);
        char name[20];
        long studentnum;
public:
	
	/* UCSDStudent default constructor */
	UCSDStudent (char * nm, long stunum) : studentnum (stunum) {
		strcpy(name, nm);
	}
	
	~UCSDStudent(void) {}

	/* Operator const char * -- returns name of student */
	operator const char * (void) const {
		return name;
	}
	/* operator == -- compares ucsd student objects */
	long operator == (const UCSDStudent & student) const {
		return ! strcmp (name, student);
	}
	/* operator < -- compares ucsd student objects */
	long operator < (const UCSDStudent & student) const {
		return (strcmp(name, student) < 0) ? 1 : 0;
	}
};

#endif
