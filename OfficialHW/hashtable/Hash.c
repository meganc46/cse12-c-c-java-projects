/****************************************************************************
 
Megan Chang
CSE 12, Fall 2015
Nov 4 2015
cs12xee
Assignment Six

Filename: Hash.c
Descriptoin: Creates hash table in form of array, as well as functions to 
			insert and lookup elements in the table.

*****************************************************************************/


#include <cstdlib>
#include <string.h>
#include "Hash.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

using namespace std;

/* debug messages */
static const char DEBUG_ALLOCATE[] = " - Allocated]\n";
static const char DEBUG_INSERT[] = " - Insert]\n";
static const char DEBUG_LOCATE[] = " - Locate]\n";
static const char DEBUG_LOOKUP[] = " - Lookup]\n";
static const char FULL[] = " is full...aborting...]\n";
static const char HASH[] = "[Hash Table ";
static const char HASH_VAL[] = "[Hash value is: ";
static const char INSERT_BUMP[] = "[bumping to next location...]\n";
static const char PROCESSING[] = "[Processing ";
static const char TRYING[] = "[Trying index ";


long HashTable :: debug = 0;
int HashTable :: counter = 0;


void HashTable :: Set_Debug (long option)

/***************************************************************************
% Routine Name : HashTable :: Set_Debug (public)
% File :         Hash.c
% 
% Description :  This function sets debug mode on or off
%
% Parameters descriptions :
% 
% name               description
% ------------------ ------------------------------------------------------
% option             true should set debug mode on, false should set debug
%                    mode off.
***************************************************************************/

{
	/* 1 is true, 0 is false */
	debug = option;
}


HashTable :: HashTable (long sz) : size (sz),
        table_count(++counter), occupancy (0), table (new Base *[sz])
/***************************************************************************
% Routine Name : HashTable :: HashTable (public)
% File :         Hash.c
% 
% Description :  This function allocates an initializes the memory
%                associated with a hash table.
%
% Parameters descriptions :
% 
% name               description
% ------------------ ------------------------------------------------------
% size               The number of elements for the table...MUST BE PRIME!!!
***************************************************************************/

{
	/* loops through table, sets index location to null */
	for (int i = 0; i < size-1; i++){
		table[i] = NULL;
	}
	if (debug)
		cerr << HASH << table_count << DEBUG_ALLOCATE;

}	/* end: HashTable */


HashTable :: ~HashTable (void)
/***************************************************************************
% Routine Name : HashTable :: ~HashTable  (public)
% File :         Hash.c
% 
% Description :  deallocates memory associated with the Hash Table.  It
%                will also delete all the memory of the elements within
%                the table.
***************************************************************************/

{
	/* loops through table, deleting each element at index */
	for (int i = 0; i < size; i++){
		delete(table[i]);
	}

	/* delete table object */
	delete(table);	
}	/* end: ~HashTable */


long HashTable :: Insert (Base * element, long recursiveCall)
/***************************************************************************
% Routine Name : HashTable :: Insert (public)
% File :         Hash.c
% 
% Description : This function will insert the element in the hash table.
%		If the element cannot be inserted, false will be returned.
                If the element can be inserted, the element is inserted and 
		true is returned. Duplicate insertions will cause the existing
		element to be deleted, and the duplicate element to take its
		place.
%
% Parameters descriptions :
%  
% name               description
% ------------------ ------------------------------------------------------
% element            The element to insert.
% recursiveCall      Whether the call to this function is recursive or not.
% <return>           1 or 0 indicating success or failure of insertion
***************************************************************************/

{	
	/* if recursively called */
	if (recursiveCall){
		if (debug)
			cerr << HASH << table_count << DEBUG_INSERT;
		
		/* call locate on element */
		Locate(element);
	}

	/* else if non-recursively called */
	else {

		/* set index to non-reachable index */
		index = -1;
		if (debug)
			cerr << HASH << table_count << DEBUG_INSERT;
		
		/* call locate on element */
		Locate(element);
	}

	/* ifplace to be inserted into is empty */
	if (table[index] == NULL){

		/* insert element into that place */
		table[index] = element;

		/* increment occupancy */
		occupancy++;

		/* exit fxn */
		return true;
	}

	/* if element at current index is equal to element to be inserted */
	if (*table[index] == *element){
		/* replace element already in table with new element to be inserted */
		table[index] = element;
		return true;
	}

	/* if table is full */
	if (occupancy == size){
		if (debug)
			cerr << HASH << table_count << FULL;
		return false;
	}

	/* if table is not full */
	else {
		if (debug)
			cerr << INSERT_BUMP;
		
		/* creates varaible to hold bumped element later on */
		Base * bumped;

		/* initialises variable to element currently at loc index in table */
		bumped = table[index];

		/* inserts element into table at current index */
		table[index] = element;

		/* call insert again on bumped element recurisvely */
		Insert(bumped, true);
	}
    return true;
}


const Base * HashTable :: Locate (const Base * element) const
/***************************************************************************
% Routine Name : HashTable :: Locate (private)
% File :         Hash.c
% 
% Description : This function will locate the location in the
                table for the insert or lookup.
%
% Parameters descriptions :
%  
% name               description
% ------------------ ------------------------------------------------------
% element            The element needing a location
% <return>           item found, or null if not found

***************************************************************************/

{
	if (debug) {
		cerr << HASH << table_count << DEBUG_LOCATE;
		cerr << PROCESSING << (const char *)(*element) << "]" << "\n";
    }

	/* converst name to ascii sum of its characters */
	int ascii_sum = (long)(*element);
	if (debug)
		cerr << HASH_VAL << ascii_sum << "]" << "\n";
	
	/* sets increment to following expression */
	int increment = (ascii_sum % (size-1)) + 1;

	/* if index is -1, set index to expected initial value based on alc */
	if (index == -1){
		index = ascii_sum % size;
	}

	/* if index not equal to -1, increment index */
	else {
		index = (index + increment) % size;
	}

	/* loops and increments index */
	for (int i = 0; i < size; i++){
		if (debug)
			cerr << TRYING << index << "]" << "\n";

		/* if current index in table is empty, or current element in index in
		 * table is less than element to be inserted/lookd up, return null */
		if (table[index] == NULL || *table[index] < *element)
			return NULL;

		/* if element at current element in table is equal to element to be 
		 * inserted or looked up, return that element */
		if (*table[index] == *element)
			return table[index];

		/* increment index */
		index = (index + increment) % size;

	}
	return NULL;
}


const Base * HashTable :: Lookup (const Base * element) const
/***************************************************************************
% Routine Name : HashTable :: Lookup (public)
% File :         Hash.c
% 
% Description : This function will lookup the element in the hash table.  If
%               found a pointer to the element is returned.  If the element
%               is not found, NULL will be returned to the user.
%
% Parameters descriptions :
%  
% name               description
% ------------------ ------------------------------------------------------
% element            The element to insert or to lookup.
% <return>           A pointer to the element if found, else NULL
***************************************************************************/

{
	if (debug)
		cerr << HASH << table_count << DEBUG_LOOKUP;
	
	/* sets index to -1 so it is unreachable */
	index = -1;

	/* if return value of locate(element) is equal to element to lookup,
	 * aka elemtn we are looking for is found */
	if (*Locate(element) == *element){

		/* return found element */
		return table[index];
	}

	/* if element is not found */
	else {

		/* return null */
		return NULL;
	}
}


ostream & HashTable :: Write (ostream & stream) const
/***************************************************************************
% Routine Name : HashTable :: Write (public)
% File :         hash.c
% 
% Description : This funtion will output the contents of the hash table
%               to the stream specificed by the caller.  The stream could be
%               cerr, cout, or any other valid stream.
%
% Parameters descriptions :
% 
% name               description
% ------------------ ------------------------------------------------------
% stream             A reference to the output stream.
% <return>           A reference to the output stream.
***************************************************************************/

{
	stream << "Hash Table " << table_count << ":\n"
		<< "size is " << size << " elements, "
		<< "occupancy is " << occupancy << " elements.\n";

	/* go through all table elements */
	for (long index = 0; index < size; index++)
		if (table[index])
			table[index]->Write(stream << "at index "
			<< index << ":  ") << "\n";
	return stream;
}	/* end: Write */

