/**
 * Megan Chang
 * CSE 12, Fall 2015
 * Nov 4, 2015
 * cs12xee
 * Assignment Six
 *
 * Filename: HashTable.java
 * Description: Creates hash table in form of array, as well as functions to
 *				look up and insert elements in the table.
 *
 * @author Megan Chang (cs12xee)
 */ 
public class HashTable extends Base {

	/* counters, flags and constants */
	private static int counter = 0;         // number of HashTables so far
	private static boolean debug;           // allocation of debug states
	protected static final short NULL = 0;  // in case you want to use NULL

	/* data fields */
	private long occupancy;         // number of items stored in table
	private int size;               // size of hash table
	private Base table[];   // the Hash Table itself ==> array of Base
	private int tableCount; // which hash table it is 

	/* initialized by Locate function */
	private int index;      // last location checked in hash table

	/* debug messages */
	private static final String DEBUG_ALLOCATE = " - Allocated]\n";
	private static final String DEBUG_INSERT = " - Insert]\n";
	private static final String DEBUG_LOCATE = " - Locate]\n";
	private static final String DEBUG_LOOKUP = " - Lookup]\n";
	private static final String FULL = " is full...aborting...]\n";
	private static final String HASH = "[Hash Table ";
	private static final String HASH_VAL = "[Hash value is: ";
	private static final String INSERT_BUMP = 
		"[bumping to next location...]\n";
	private static final String PROCESSING = "[Processing "; 
	private static final String TRYING = "[Trying index "; ; 

	/**
	 * Function:	setDebugOff
	 * Purpose:		to set debug status
	 * Description:	sets debug status to false
	 * Input:		none
	 * Result:		returns nothing
	 */
	public static void setDebugOff () {
		debug = false;
	}       

	/**
	 * Function:	setDebugOn
	 * Purpose:		to set debug status
	 * Description:	sets debug status to true
	 * Input:		none
	 * Result:		returns nothing
	 */
	public static void setDebugOn () {
		debug = true;
	}       

	/**
	 * Function:	HashTable 
	 * Purpose:		Allocates and initializes the memory associated with a hash
	 *				table
	 * Description: Initialises variables, creates table, sets all indices in 
	 *				table to null
	 * Input:		sz = size of hashtable
	 * Result:		new HashTable constructed with all indices empty
	 *
	 * @param  sz   << any positive prime number >>
	 */
	public HashTable (int sz) {
		
		// creates/initialises new Base object of size sz
		table = new Base[sz];
		
		// sets occupancy to 0
		occupancy = 0;

		// sets tableCount to pre-incremented counter
		tableCount = ++counter;

		// sets size to parameter sz
		size = sz;

		// loop that sets all locations in table to null
		for (int i = 0; i < size-1; i++){
			table[i] = null;
		}

		// debug statement
		if (debug == true)
			System.err.print(HASH + tableCount + DEBUG_ALLOCATE);
	}

	/**
	 * Performs insertion into the tabble via delegation to the
	 * private insert method.
	 *
	 * @param   element        << Base object element to be inserted >>
	 * @return  << true for successful insert, false if not >>
	 */
	public boolean insert (Base element) {

		// calls another insert function that has option to recursively insert
		return insert (element, false);
	}

	/**
	 * Inserts the element in the hash table; if the element cannot be inserted,
	 * false will be returned; if the element can be inserted, the element is 
	 * inserted and true is returned; duplicate insertions will cause the 
	 * existing element to be deleted, and duplicate element to take its place.
	 *
	 * @param   element        << complete element to be inserted >>
	 * @param   recursiveCall  << true if fxn called recursively, false if not >>
	 * @return  << true for sucessful insert, false if not >>
	 */
	private boolean insert (Base element, boolean recursiveCall) {

		// if recursive call
		if (recursiveCall){
			if (debug)
				System.err.print(HASH + tableCount + DEBUG_INSERT);
			
			// call locate on element
			locate(element);
		}

		// else if non-recurisve call
		else {

			// set index to non-rechable index
			index = -1;
			if (debug)
				System.err.print(HASH + tableCount + DEBUG_INSERT);
			
			// call locate on element
			locate(element);
		}

		// if place to be inserted into is empty
		if (table[index] == null){

			// insert element into that place
			table[index] = element;

			// increment occupancy
			occupancy++;
			
			// exit fxn
			return true;
		}

		// if element at current index is equal to element to be inserted
		if (table[index].equals(element)){

			// replace element already in table with new element to be inserted
			table[index] = element;
			return true;
		}

		// if table is full
		if (occupancy == size){
			if (debug)
				System.err.print(HASH + tableCount + FULL);
			return false;
		}

		// if table is not full
		else {
			if (debug)
				System.err.print(INSERT_BUMP);
			
			// creates variable to hold bumped eleement later on
			Base bumped;

			// intialises variable to element currently at loc index in table
			bumped = table[index];

			// inserts element into table at current index
			table[index] = element;

			// call insert again on bumped element recursively
			insert(bumped, true);
		}
		return true;
	}

	/**
	 * Locates the index in the table where the insertion is to be performed, an
	 * item is found, or an item is determined not to be there; sets the 
	 * variable index to the last location checked; it will be used by insert 
	 * and lookup; returns the item if an item with matching name is found; 
	 * otherwise, return null; loops stops when encountering a null table 
	 * location or when encountering an item that is smaller than the parameter
	 * item.
	 *
	 * @param   element  << complete and incomplete elements, depends on if
	 *						called from insert of lookup >>
	 * @return  << returns item if item with matching name is found, otherwise 
	 *			   returns null >>
	 */
	private Base locate (Base element) {
		if (debug){
			System.err.print(HASH + tableCount + DEBUG_LOCATE);
			System.err.println(PROCESSING + element.getName() + "]");
		}

		// converts name to ascii_sum of its characters, calling hashCode from
		// driver.java, stores this in variable
		int ascii_sum = element.hashCode();
		if (debug)
			System.err.println(HASH_VAL + ascii_sum + "]");
		
		// sets increment to following expression
		int increment = (ascii_sum % (size -1)) + 1;

		// if index is -1, set index to expected initial value based on alg
		if (index == -1){
			index = ascii_sum % size;
		}

		// if index is not -1, increment index
		else {
			index = (index + increment) % size;
		}

		// loops and increments index
		for (int i = 0; i < size; i++){
			if (debug){
				System.err.println(TRYING + index + "]");
			}
			
			// if current index in table is empty, or current elment in index in
			// table is less than element to be inserted/looked up, return null
			if (table[index] == null || (table[index].isLessThan(element)))
				return null;

			// if element at current element in table is equal to element to be 
			// inserted or lookup, return that element
			if (table[index].equals(element))
				return table[index];

			// increment index
			index = (index + increment) % size;

		}
		return null;

	}
	/**
	 * Looks up the element in the hash table; returns pointer to the element if
	 * found, null otherwise
	 *
	 * @param   element  << Incomplete element (name, no number) >>
	 * @return  << returns ptr to element if found, null otherwise >>
	 */
	public Base lookup (Base element) {
		if (debug)
			System.err.print(HASH + tableCount + DEBUG_LOOKUP);
		
		// sets index to -1
		index = -1;

		// if return value of locate(element) is equal to element to lookup, 
		// aka element we are looking for is found
		if (locate(element).equals(element)){

			// return element
			return table[index];
		}

		// if element is not found
		else {

			// return null
			return null;
		}
	}

	/**
	 * Creates a string representation of the hash table. The method 
	 * traverses the entire table, adding elements one by one
	 * according to their index in the table. 
	 *
	 * @return  String representation of hash table
	 */
	public String toString () {

		String string = "Hash Table " + tableCount + ":\n";
		string += "size is " + size + " elements, "; 
		string += "occupancy is " + occupancy + " elements.\n";

		/* go through all table elements */
		for (int index = 0; index < size; index++) {

			if (table[index] != null) {
				string += "at index " + index + ":  ";
				string += "" + table[index] + "\n"; 
			}
		}

		return string;
	}
}
