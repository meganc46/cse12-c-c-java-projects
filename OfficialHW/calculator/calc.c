/******************************************************************************

  Megan Chang
  CSE 12, Fall 2015
  Oct 21, 2015
  cs12xee
  Assignment Four

Filename: calc.c
Description: this program is a command-line calculator, adding, subtracting,
multiplying, dividing, xponentitationg, and factorialing operands using 
paralell stacks.

 ******************************************************************************/


#include <ctype.h>
#include <stdio.h>
#include "calc.h"
#include "mylib.h"
#include "stack.h"

#define CHARACTER(word) ((char) ((word) & 0x000000FF))
#define INDEX(word) (((word) & 0x0000FF00) >> 8)
#define PRIORITY(word) ((word) & 0x0000FE00)
#define BYTE 8                
/* shifting bits in long 63 to the left */
#define SIGN_BIT (1L << ((sizeof(long) << 3) -1))

static char operators[] = "()+-*/^ !";

static long add (long, long);
static long divide (long, long);
static long exponent (long, long);
static long fact (long, long);
static long mult (long, long);
static long setupword (int);
static long sub (long, long);
static const char openp = '(';  /* open parenthesis */
static const char closep = ')'; /* closed parenthesis */
static const char FACT = '!';   /* factorial operator */

static long (*functions[]) (long, long) =
{ NULL, NULL, add, sub, mult, divide, exponent, NULL, fact };


/* Function:	eval
 * Purpose:		uses two stacks to evaluate mathematical expressions from 
 *				postfix notation
 * Description:	creates a local stack to be used in conjunction with stack 
 *				passed in as parameter. stack 1 is reversed onto stack 2.
 *				while stack 2 isn't empty, pop numbers from stack2 and push
 *				them onto stack 1 until the first operator is encountered, at 
 *				which point the appropriate number of opperands from stack 1
 *				is popped and evaluted using the operator popped from stack
 *				2. The result of this computation is pushed onto stack 1, and
 *				is returned at end of expression.
 * Input:		stack1 - input paramter, pointers to stack containing postfix
 *				expressions to evaluate
 * Results:		final result of expression returned as long
 */

long eval (Stack * stack1) {
	/* [remove after use]
	   ALGORITHM FOR POST-FIX EVALUATION:
	   You will need 2 stacks for this algorithm.  Your function "eval" will
	   take a pointer to a stack (stack1) as a parameter.  It will use this stack
	   as one of its needed stacks.  The other stack will be local.  After the
	   evaluation, the parameter stack will be empty.

	   Reverse stack1 onto stack2, then begin the evaluation:
	   While stack2 is not empty
	   pop numbers from stack2, pushing all digits popped to stack1.  Once a
	   non-numbers is encountered from stack2, the appropriate number of
	   operands will be popped from stack1 and evaluated using the operator
	   just popped from stack2.  The result of this computation will be
	   pushed on stack1.
	   The final result that remains on stack1 is the final result of the
	   expression.  This result is then used as the return value for this
	   function.  During the evaluation process, stack2 holds positive integers
	   and operators, but stack1 only holds signed integers.
	   */

	/* initialise variables */
	long item, tvalue, pvalue, result;
	long op1 = 0;
	long op2 = 0;

	/* creates new local stack */
	Stack * stack2 = new_Stack(CALCSTACKSIZE);

	/* reverse elements in stack1 onto stack2 */
	while (!isempty_Stack(stack1)){
		pop(stack1, &pvalue);
		push(stack2, pvalue);
	}

	/* while stack2 isn't empty... */
	while (!isempty_Stack(stack2)){

		/* peek at top element to check whether it is digit or operator */
		top(stack2, &tvalue);

			pop(stack2, &pvalue);
		
		/* if element is a digit, pop from stack2 and push to stack1 */
		if (tvalue >= 0){
			push(stack1, pvalue);

			/* peek again at new top element */
			top(stack2, &tvalue);
		}
		
		
		/* if element is a !, aka factorial operator... */
		else if (tvalue == setupword(FACT)){
			if (!isempty_Stack(stack1)){	
			/* pop the factorial from stack2 and store in item */ 
			
			pop(stack1, &op1);

			/* store result of factorial evaluation in result variable */
			result = functions[INDEX(tvalue)](op1,op2);

			/* push result onto stack1 for future use */
			push(stack1, result);
			}
		}

		/* if element is an operator...*/
		else {

			/* store operant into item variable */
			item = tvalue;

			/* pop 2 digits from stack1 to evaluate */
			pop(stack1, &op1);
			pop(stack1, &op2);

			/* evaluate expression and store result in result variable */
			result = functions[INDEX(item)](op1,op2);

			/* push result onto stack1 for future use */
			push(stack1, result);
		
		}
	}
	/* delete local stack, stack2 */
	delete_Stack(&stack2);
	empty_Stack(stack1);
	//pop(stack1, &result);
	/* return most recent result in stack1 */
	return result;	
}



/* Function:	intopost
 * Purpose:		uses 2 stacks to convert infix mathematical expression
 *				entered by user into their postfix equivalents
 * Description:	creates local stack. take in character inputs, return EOF
 *				when ctrl-D is entered, ignore blank spaces. if input is a
 *				digit, read all digits until the first non-digit, converting
 *				them to decimal numbers and storing them on stack1. if input
 *				is open parenthesis, push it to stack 2, and if input is
 *				closed parenthesis, pop elements from stack2 to stack1 until
 *				the previous matching open parenthesis is encountered, then
 *				discard the two parenthesis. if input is another operator,
 *				then keep popping from stack 2 and pushing to stack1 until
 *				encounter an operator on stack2 with a lower priority than
 *				the input, at which point you push the input onto stack2.
 *				after processing all inputs, pop remaining elemnts from stack
 *				2 and push to stack 1.
 * Input:		stack1 - output parameter, expected to be pointers to empty
 *				stacks used to store postfix expressions
 * Results:		stack 1 contains the postfix expressions in reverse order,
 *				ready to be fed into eval.
 */

long intopost (Stack * stack1) {
	/* [remove after use]
	   ALGORITHM FOR INFIX-TO-POSTFIX:
	   You will need 2 stacks for this algorithm.  Your function "intopost" will
	   take a pointer to a stack (stack1) as a parameter.  It will use this stack
	   as one of its needed stacks.  The other stack will be local.  The
	   parameter stack will also serve as the place to store the resultant
	   post-fix expression.  The return value from intopost is either EOF
	   when ^D is entered or a non-zero value indicating that the function
	   succeeded.

	   Process each character of the input in turn
	   if character is EOF, return EOF
	   if character is blank, then ignore it

	   if character is a digit, then continue to read digits until you read
	   a non-digit, converting this number to decimal as you go.  Store
	   this decimal number on stack1

	   else if character is '(' then push it to stack2

	   else if character is ')' then repeatedly pop stack2, pushing all
	   symbols popped from stack2 onto stack1 until the first '('
	   encountered is popped from stack2.  Discard '(' and ')'

	   else repeatedly push to stack1 what is popped from stack2
	   until stack2 is empty or stack2's top symbol has a lower
	   priority than the character entered.  Then push the character
	   onto stack2.

	   After processing all characters, pop anything remaining on stack2, pushing
	   all symbols popped from stack2 to stack1.  Stack1 now contains the
	   post-fix expression, in reverse order.
	   */

	/* create new local stack */
	Stack * stack2 = new_Stack(CALCSTACKSIZE);

	/* gets input for first time from stdin */
	long input = getchar();

	/* initialise variables */
	long pvalue, tvalue;

	/* while input is not newline */
	while (input != '\n'){

		/* if input is ctrlD, delete stack2 and return EOF, terminating
		 * program */
		if (input == EOF){
			delete_Stack(&stack2);
			return EOF;
		}

		/* if input is space, ignore and keep going */
		if (isspace(input)){
			input = getchar();
			continue;
		}

		/* if input is digit...*/
		if (isdigit(input)){

			/* put digit back onto stdin buffer */
			ungetc(input, stdin);

			/* assign number converted to decimal to variable number */
			long number =  decin();

			/* push number onto stack1 */
			push(stack1, number);
		}

		/* if input is open parenthesis, convert it to differnt format by
		 * calling setupword, then pushing it onto stack2 */
		else if (input == openp){
			push(stack2, setupword(input));
		}

		/* if input is closed parenthesis...*/
		else if (input == closep){

			/* pop from stack2*/
			pop(stack2, &pvalue);

			/* while popped element is not open parenthesis...*/
			while (pvalue != setupword(openp)){

				/* push the element onto stack1*/
				push(stack1, pvalue);

				/* pop the next element from stack2*/
				pop(stack2, &pvalue);
			}
		}

		/* if input is any other operator */
		else {

			/* if stack2 isn't empty, peek at top element in stack2 */
			if (!isempty_Stack(stack2))
				top(stack2, &tvalue); 

			/* while stack 2 isn't empty and priority of input is greater
			 * than priority of top element in stack2...*/
			while (!isempty_Stack(stack2) && 
					PRIORITY(tvalue) >= PRIORITY(setupword(input))) {
				if (tvalue == fact){
					break;
				}

				/* pop element from stack2 and push onto stack1 */
				pop(stack2, &pvalue);
				push(stack1, pvalue);

				/* if stack2 isn't empty, peek at top element again*/
				if (!isempty_Stack(stack2)){
					top(stack2, &tvalue);
				}
			}

			/* push operator input onto stack2 */
			push(stack2, setupword(input));
		}

		/* read in next input */
		input = getchar();
	}

	/* while stack2 isn't empty, pop all elements from it and push onto
	 * stack1 */
	while (!isempty_Stack(stack2)){
		pop(stack2, &pvalue);
		push(stack1, pvalue);
	}

	/* delete stack2 */
	delete_Stack(&stack2);

	/* if infix to postfix conversion successful, return 1*/
	return 1;
}

static long add (long augend, long addend) {
	return augend + addend;
}

static long divide (long divisor, long dividend) {
	return dividend / divisor;
}

/* Function:	exponent
 * Purpose:		evalute epxressions with exponents
 * Description:	initalise variable to act as counter for power. if power is
 *				0, result is 1. if power is not zero, multiply base by itself
 *				power number of times, and decrement power after every
 *				multiplication
 * Input:		power - any positive integer
 *				base - any positive integer
 * Results:		result is a base multiplied by itself power number of times,
 *				which is the function of an exponent, and result is returned.
 */

static long exponent (long power, long base) {

	/* initialise result to 1 to begin with */
	long exp = 1;

	/* if power is 0, set result to 1 and return  */
	if (power == 0){
		exp = 1;
		return exp;
	}

	/* if power is not zero...*/
	while (power != 0){

		/* multiply exp by base */
		exp = exp * base;

		/* decrement power */
		power--;
	}

	/* result is exp, which is the base multiplied by itself power number of
	 * times */
	return exp;
}

/* Function:	fact
 * Purpose:		recursively calculates factorial expression
 * Description:	if input paramter xxx is zero, it returns one and terminates. if
 *				xxx is not zero, then a recursive function call is returned, 
 *				each time xxx is being mulitplied by xxx-1, which is the 
 *				definition of a recurisve function.
 * Input:		xxx - any positive integer
 *				ignored - any valid integer (not magic #)
 * Results:		the final value of xxx, which is xxx!, is returned
 */

static long fact (long xxx, long ignored) {

	/* if xxx is 0, then return 1 and exit out of method */
	if (xxx == 0)
		return 1;

	/* if xxx is not 0, then call the fact recursively */
	else 
		/*xxx is being multiplied by xxx-1 after every call */
		return (xxx * fact(xxx-1, ignored));
}

static long mult (long factorx, long factory) {
	return factorx * factory;
}

/* Function:	setupword
 * Purpose:		constructor fxn for longs representing operators to be stored
 *				on the stack
 * Description:	converts the operator to
 */

static long setupword (int character) {
	/* YOUR CODE GOES HERE */
	long index = 0;
	while (operators[index] != character) {
		index++;
	}
	return SIGN_BIT | (index << BYTE) | character;;
}

static long sub (long subtrahend, long minuend) {
	return minuend - subtrahend;
}

