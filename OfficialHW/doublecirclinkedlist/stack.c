/*************************************************************

Megan Chang (author of headers and functions called only) 
CSE 12, Fall 2015
Oct 29 2015
cse12xee
Assignment Five

Filename:		stack.c
Desciption:		stack that implements linked list (list.c)

*************************************************************/

#include <stdio.h>
#include "list.h"
#include "stack.h"

/* Function:		delete_Stack
 * Purpose:			to delete the current stack
 * Description:		calls delete_List from list.c, 
 * Input:			passing in a double pointer to the stack as a paramter
 * Result:			stack is deleted, nothing returned
 */

void delete_Stack (Stack ** spp) {
        delete_List (spp);
}

/* Function:		isempty_Stack
 * Purpose:			checks if current stack is empty
 * Description:		calls isempty_List in list.c
 * Input:		    pointer to current stack, this_stack
 * Result:			long returned - 1 if empty, 0 if not
 */

long isempty_Stack (Stack * this_stack) {
        return isempty_List (this_stack);
}

/* Function:		new_Stack
 * Prpose:			to create a new stack
 * Description:		calls new_List from list.c
 * Input:			copy_func - fxn that returns void ptr
 *					delete_func - fxn that returns void ptr
 *				    write_fucn - fxn that returns void ptr
 * Result:			returns pointer to newly created stack
 */

Stack * new_Stack (void *(*copy_func) (void *),
        void (*delete_func) (void *),
        FILE *(*write_func) (void *, FILE *)) {

        return new_List (copy_func, delete_func, write_func);
}

/* Function:		pop
 * Purpose:			to pop element from stack
 * Descriptoin:		calls remove_List from list.c, speifically removing the 
 *					last element
 * Input:			pointer to current stack to be popped from
 * Result:			void ptr to popped element is returned
 */

void * pop (Stack * this_stack) {
        return remove_List (this_stack, END);
}

/* Function:		push
 * Purpose:			to push element onto stack
 * Description:		insert is called from list.c, specifically inserting at 
 *					the end of the list
 * Input:			pointer to the current stack to be inserted into
 *					pointer to element to push
 * Result:			1 returned for successful push, 0 for not successful
 */

long push (Stack * this_stack, void * element) {
        return insert (this_stack, element, END);
}

/* Function:		top
 * Purpose:			to peek at top element on stack
 * DEscription:		calls view from list.c, specifically viewing last element 
 * Input:			pointer to stack to be topped from
 * Result:			pointer to topped element returned
 */

void * top (Stack * this_stack) {
        return view (this_stack, END);
}

/* Function:		write_Stack
 * Purpose:			to print out element in stack to filestream stream
 * Description:		calls write_List from list.c
 * Input:			pointer to stack whose elements we want to view
 *					pointer to filestream stream
 * Result:			returns pointer to filestream, prints out stack
 */

FILE * write_Stack (Stack * this_stack, FILE * stream) {
        return write_List (this_stack, stream);
}
