/****************************************************************************

Megan Chang
CSE 12, Fall 2015
Oct 29 2015
cse12xee
Assignment Five

Filename:		list.c
Descriptoin:	this program is a linked list that is used as a base to 
				implement calc.c

DEBUG QUESTION:

1. end: 0x6040f0
2. Node 1 data: 0x604050
3. Node 1 pre: 0x6040b0
4. Node 1 next: 0x604070
5. Node 2 data: 0x604090
6. Node 2 pre: 0x6040b0
7. Node 2 next: 0x604070
8. Node 3 data: 0x6040d0
9. Node 3 pre: 0x6040b0
10. Node 3 next: 0x604070

****************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include "mylib.h"
#include "list.h"

typedef struct Node {
	struct Node * pre;      /* how to access the prior Node */
	struct Node * next;     /* how to access the next Node */
	void * data;            /* the data to store */
} Node;

typedef struct List {
	Node * end;             /* the end of the list */
	long list_count;        /* which list is it */
	long occupancy;         /* current size of list */
	void *(*copy_func) (void *);
	void (*delete_func) (void *);
	FILE *(*write_func) (void *, FILE *);
} List;

/* private Node function declarations */
static long check_to_go_forward (List * this_list, long where);
static void delete_Node (Node **, void (*delete_func) (void *));
static Node * find_location (List * this_list, long where);
static Node * insert_Node (Node *, void *, void *(*copy_func) (void *)); 
static Node * new_Node (void *, void *(*copy_func) (void *));
static void * remove_Node (Node *);
static void * view_Node (Node *); 
static FILE * write_Node (Node *, FILE *, FILE *(*write_func) (void *, FILE *));

/* catastrophic error messages */
static const char ADNEXT_NONEXIST[] = 
"Advance next from non-existent list!!!\n";
static const char ADNEXT_EMPTY[] = 
"Advance next from empty list!!!\n";
static const char ADPRE_NONEXIST[] = 
"Advance pre from non-existent list!!!\n";
static const char ADPRE_EMPTY[] = 
"Advance pre from empty list!!!\n";
static const char CHECK_NONEXIST[] =
"Checking direction on non-existent list!!!\n";
static const char CHECK_EMPTY[] =
"Checking direction on empty list!!!\n";
static const char DELETE_NONEXIST[] =
"Deleting from non-existent list!!!\n";
static const char DELETE_NONEXISTNODE[] =
"Deleting a non-existent node!!!\n";
static const char ISEMPTY_NONEXIST[] =
"Is empty check from non-existent list!!!\n";
static const char INSERT_NONEXIST[] =
"Inserting to a non-existent list!!!\n";
static const char REMOVE_NONEXIST[] =
"Removing from non-existent list!!!\n";
static const char REMOVE_EMPTY[] =
"Remove from empty list!!!\n";
static const char VIEW_NONEXIST[] = 
"Viewing a non-existent list!!!\n";
static const char VIEW_NONEXISTNODE[] = 
"Viewing a non-existent node!!!\n";
static const char VIEW_EMPTY[] =
"Viewing an empty list!!!\n";
static const char WRITE_NONEXISTFILE[] =
"Writing to a non-existent file!!!\n";
static const char WRITE_NONEXISTLIST[] =
"Writing from a non-existent list!!!\n";
static const char WRITE_MISSINGFUNC[] =
"Don't know how to write out elements!!!\n";
static const char WRITE_NONEXISTNODE[] =
"Writing from a non-existent node!!!\n";

/* debug messages */
static const char ADNEXT[] = "[List %ld - Advancing next]\n";
static const char ADPRE[] = "[List %ld - Advancing pre]\n";
static const char INSERT[] = "[List %ld - Inserting node]\n";
static const char REMOVE[] = "[List %ld - Removing node]\n";
static const char VIEW[] = "[List %ld - Viewing node]\n";
static const char LIST_ALLOCATE[] = "[List %ld has been allocated]\n";
static const char LIST_DEALLOCATE[] = "[List %ld has been deallocated]\n";

static int debug_on = FALSE;    /* allocation of debug flag */
static long list_counter = 0;   /* the number of lists allocated so far */
Node * temp_end;

/* Function:	set_debug_off
 * Purpose:		to set debug msg off
 * Description:	set debug to off
 * Inputs:		void
 * Results:		returns nothing, sets debug to off
 */

void set_debug_off (void) {
	debug_on = FALSE;

}

/* Function:	set_debug_on
 * Purpose:		to set debug msg on
 * Description:	set debug to on
 * Inputs:		void
 * Results:		returns nothing, sets debug to on if run with -x
 */

void set_debug_on (void) {
	debug_on = TRUE;
}

/* Function:	advance_next_List
 * Purpose:		shifts elements of list backwards by one
 * Description:	moves end pointer of this_list to move forwrad by one node
 * Input:		pointer to the list
 * Results:		elements in list shifted backwards, returns nothing
 */

void advance_next_List (List * this_list) {

	/* if list doesn't exist... */
	if (this_list == NULL){

		/* error statement */
		fprintf(stderr, ADNEXT_NONEXIST);
	}

	/* if list's end pointer doesn't exist, aka if list is empty */
	else if (this_list->end == NULL)
		fprintf(stderr, ADNEXT_EMPTY);

	/* if list exists and is not empty */
	else {

		/* set list's end pointer to next pointer in list */
		this_list->end = this_list->end->next;
		if (debug_on)
			fprintf (stderr, ADNEXT, list_counter);
	}
}

/* Function:	advance_pre_List 
 * Purpose:		shifts elements of list forwrad by one
 * Description:	moves end pointer of this_list backward by one node
 * Input:		pointer to the list
 * Results:		elements in list shifted forwards, return nothing
 */

void advance_pre_List (List * this_list) {

	/* if list doesn't exist... */
	if (this_list == NULL)
		fprintf(stderr, ADPRE_NONEXIST);

	/* if list is empty... */
	else if (this_list->end == NULL)
		fprintf(stderr, ADPRE_EMPTY);

	/* if list exists and is not empty */
	else {

		/* set lists's end pointer to previous pointer in list */
		this_list->end = this_list->end->pre;
		if (debug_on)
			fprintf(stderr, ADPRE, list_counter);
	}
}

/* Function:	check_to_go_forward
 * Purpose:		checks whether it would be more efficient to reach item
 *				where in the list by looping forwards from end of list or
 *				backwards from end of list
 * Description:	sets threshold value to middle of list, then compares
 *				value of where to threshold value. If where is before
 *				threshold, then loop from front, if not, loop from back
 * Inputs:		pointer to this_list being checked
 *				long where - location in list inputted by user
 * Results:		returns 1 if going forward is faster, and returns 0 if
 *				backwards is faster
 */

static long check_to_go_forward (List * this_list, long where) {

	/* if list doesn't exist */
	if (this_list == NULL)
		fprintf(stderr, CHECK_NONEXIST);

	/* if list isn't empty */
	if (this_list->occupancy != 0)
		fprintf(stderr, CHECK_EMPTY);

	/* if list exists and is not empty */
	else {

		/* sets threshold value to half of # elements in list */
		int thresh = this_list->occupancy / 2;

		/* if threshold value is in front of where value, will approach where 
		 * faster from front, so return 1 */
		if (thresh > where)
			return 1;

		/* else return 0 */
		else
			return 0;
	}
}

/* Function:	delete_List
 * Purpose:		destructor function that deallocates all memeory associated with
 *				the list, including memory associated with all of nodes in list.
 *				sets list pointer in calling function to NULL
 * Description:	sets up loop of calling delete_Node as go forward from Node  to 
 *				Node, then deletes one more time outside of loop
 * Input:		double pointer to list
 * Results:		list and nodes in it are deleted
 */

void delete_List (List ** lpp) {

	/* if list doesn't exist */
	if (*lpp == NULL)
		fprintf(stderr, DELETE_NONEXIST);

	/* declare and initliase ptr vars */
	List * this_List;
	this_List = *lpp;
	Node * working; 

	/* if list is not empty */
	if (this_List->occupancy != 0){

		/* set working to node following end node in list */
		working = this_List->end->next;

		/* while list has more than 1 node */
		while (this_List->occupancy > 1){

			/* call delete_Node on end node */
			delete_Node(&working->pre, this_List->delete_func);

			/* decrement occupancy */
			this_List->occupancy--;

			/* check again if list now has more than 1 node */
			if (this_List->occupancy > 1)

				/* set working to next node after it */
				working = working->next;
		}

		/* call delete_Node once more to remove last remaning node */
		delete_Node(&working, this_List->delete_func);
	}

	/* free memory associated with the list */
	free (this_List);

	/* sets pointer to this list to null */
	this_List = NULL;

	if (debug_on)
		fprintf(stderr, LIST_DEALLOCATE, list_counter);

	/* decrement list counter */
	list_counter--;
}

/* Function:	find_location
 * Purpose:		findds location at which we wish to insert, remove, or view a 
 *				node, used to locate the Node at location where
 * Description:	changes end pointer of list to where, storing original end
 *				pointer in anoter variable to be restored later on. IT calls 
 *				check_to_go_forward to find quickest direction to where, then 
 *				calls advance_next/pre_list to change end pointer.
 * Input:		this_list - pointer to this list
 *				where - location we want to go to
 * Results:		returns a Node pointer, which is this_list's end pointer
 *				this_list end is also set to location where 
 */

static Node * find_location (List * this_list, long where) {

	/* loop counter variables declared here */
	int i,j;

	/* if looping forwards is faster, then call advance_next_list wherenumber of
	 * times */
	if (check_to_go_forward){
		for (i = 0; i < where; i++){
			advance_next_List(this_list);
		}
	}

	/* if looping backwards is faster, then call advance_pre_list where number 
	 * of times */
	else {
		for (j = 0; j < (this_list->occupancy - where); j++){
			advance_pre_List(this_list);
		}
	}

	/* returns a pointer to modified end node in list */
	return this_list->end;
}

/* Function:	insert
 * Purpose:		inserts element into this_list at locaiton where
 * Description:	calls find location to locate node at location where, then
 *				calls insert_node to incorporate node into the list. saves
 *				original end pointer in temp pointer variable, and after
 *				new node is inserted, resets end pointer to newly inserted
 *				node (if inserted at end of list) or original pointer
 *				(otherwise).
 * Input:		this_list - pointer to the list inserting into
 *				element - value to be inserted into list
 *				where - location to be inserted into
 * Results:		if insert successful, return 1, otherwise return 0
 */

long insert (List * this_list, void * element, long where) {

	/* store this_list->end in temppointer, and reset to temp pointer
	 * in find location instead of here, and then just call findloc
	 * without assigning it to anything. also need to make another
	 * return var to store new this_list->end to pass into insertnode's
	 * first paramer
	 */

	/* declare Node pointer variable to be returned later */
	Node * inserted_Node;

	/* if this list doesnt exist */
	if (this_list == NULL)
		fprintf(stderr, INSERT_NONEXIST);

	/* if where is greater than # of nodes in list, then set where * to 0, 
	 * so insert at end */
	if (where > this_list->occupancy)
		where = 0;

	/* if list is not empty */
	if (this_list->occupancy != 0) {

		/* save original end of list in temporary ptr var */
		temp_end = this_list->end;


		/* if where is 0... */
		if (where == 0)

			/* reset end pointer of list to where, determined by calling  
			 * findlocation */
			this_list->end = find_location(this_list, where);

		/* if where is anything else */
		else 
			/* fine location where also by calling find_location but with 
			 * different where parameter */
			this_list->end = find_location(this_list, where-1);
	}

	/* calls insert_Node to insert node and stores in node pointer */
	inserted_Node = insert_Node(this_list->end, element, this_list->copy_func);

	/* debug msg */
	if (debug_on)
		fprintf(stderr, INSERT, list_counter);

	/* if list is empty or where is 0... */
	if (this_list->occupancy == 0 || where == 0)

		/* set end pointer to point to newly inserted node */
		this_list->end = inserted_Node;

	/* otherwise ...*/
	else 

		/* set end pointer to point to original end ptr */
		this_list->end = temp_end;

	/* increment occupancy */
	(this_list->occupancy)++;
}

/* Function:		isempty_List
 * Purpose:			check if list is empty
 * Desription:		uses occupancy variable to see if list is empty
 * Input:			pointer to list
 * Results:			returns 1 for empty, returns 0 for not empty
 */

long isempty_List (List * this_list) {

	/* if list doesn't exist */
	if (this_list == NULL)
		fprintf(stderr, ISEMPTY_NONEXIST);

	/* if list is empty */
	if (this_list->occupancy == 0)
		return 1;
	else
		return 0;
}

/* Function:		new_List
 * Purpose:			to create new Linked list
 * Description:		constructor fxn allocates and initialises a new list object
 *					it initialises the list data fields including end, listcoun
 *					, occupancy, copy_func, delete_func, and write_func.
 * Input:			copy_func - fxn that returns void ptr, makes copy of element
 *					write_func - fxn that writes to file, returns file pointer
 *					delete_func - fxn that deletes element, returns vodi ptr
 * Results:			pointer to newly created list is returned
 */

List * new_List (
		void *(*copy_func) (void *),
		void (*delete_func) (void *),
		FILE *(*write_func) (void *, FILE *)) {

	/* allocate */
	List * this_List = (List*)malloc(sizeof(List));

	/* initialise */
	this_List -> end = NULL;
	this_List -> list_count = ++list_counter;
	this_List -> occupancy = 0;
	this_List -> copy_func = copy_func;
	this_List -> delete_func = delete_func;
	this_List -> write_func = write_func;

	if (debug_on)
		fprintf(stderr, LIST_ALLOCATE, list_counter);

	return this_List;
}


/* Function:		remove_List
 * Purpose:			removes an element in the list
 * Description:		removes an element in the list at location where. 
 *					find_location is called to locate the Node at location
 *					where, and then remove_Node is called to restructure list 
 *					to remove the node
 * Input:			pointer to the list, location where to be removed from
 * Resutls:			returns a pointer to the removed node's data
 */

void * remove_List (List * this_list, long where) {

	/* declare ptr vars */
	Node * removed_Node;
	Node * temp2;
	Node * temp_end2;

	/* intialise temp2 to pointer to current end of list */
	temp2 = this_list->end;

	/* if list doesn't exist */
	if (this_list == NULL){
		fprintf(stderr, REMOVE_NONEXIST);
		return NULL;
	}

	/* if where greater than or equal to occupancy, set it to 0 so that it will 
	 * remove from ene of list */
	if (where >= this_list->occupancy)
		where = 0;

	/* if list is not empty... */
	if (this_list->occupancy != 0){

		/* stores pointer to currend end node in temp */
		temp_end = this_list->end;

		/* stores pointer before curent end ptr in temp2 */
		temp_end2 = this_list->end->pre;

		/* if where is 0... */
		if (where == 0){

			/* sets end pointer to previous node */
			this_list->end = temp2; 
		}

		/* otherwise ... */
		else

			/* sets end pointer to node determined by calling find_loc */
			this_list->end = find_location(this_list, where);
	}

	/* otherwise if list is empty... */
	else{
		fprintf(stderr, REMOVE_EMPTY);
		return NULL;
	}

	/* calls remove_Node to remove node from current end of list, then stores
	 * the data of removed node in var to be returned */
	removed_Node = remove_Node(this_list->end);

	/* decrements occupancy */
	(this_list->occupancy)--;

	if (debug_on)
		fprintf(stderr, REMOVE, list_counter);

	/* if list is empty, set end of list to null */
	if (this_list->occupancy == 0)
		this_list->end = NULL;

	/* if where is 0, set end of list to node that came before previous end 
	 * pointer */
	else if (where == 0)
		this_list->end = temp_end2;

	/* otherwise, set end of list to previous end pointer */
	else 
		this_list->end = temp_end;

	/* return data of removed node */
	return removed_Node;
}


/* Function:		view
 * Purpose:			calls view_Node to display data of node 
 * Description:		calls find_loc to locate node at location where, then calls 
 *					view_Node to display data
 * Input:			pointer to the list
 *					where - location to be viewed
 * Results:			returns pointer to object stored at location where to view
 */

void * view (List * this_list, long where) {

	/* if list doesn't eixst */
	if (this_list == NULL)
		fprintf(stderr, VIEW_NONEXIST);

	/* if list is empty */
	if (this_list->occupancy == 0)
		fprintf(stderr, VIEW_EMPTY);

	/* declare variables */
	Node * data;
	Node * temp;

	/* store current end of list in temp var */
	temp = this_list->end;

	/* calls find location to get location where to view, end ptr is changed  */
	find_location(this_list, where);

	/* calls view Node on end node, and stores returned data in var */
	data = view_Node(this_list->end);

	/* resets end of list to previous end of list */
	this_list->end = temp;

	if (debug_on)
		fprintf(stderr, VIEW, list_counter);

	/* returns pointer to object stored at loc where */
	return data;

}

/* Function:		write_List
 * Purpose:			write elements of list
 * Description:		writes elements of list starting with first elemeent. 
 *					the list is printed to filestream stream
 * Input;			pointer to the list, pointer to the filestream
 * Result:			list is printed in termainl
 */

FILE * write_List (List * this_list, FILE * stream) {
	long count;             /* to know how many elements to print */
	Node * working;                 /* working node */

	if (!stream) {
		fprintf (stderr, WRITE_NONEXISTFILE);
		return NULL;
	}

	if (!this_list) {
		fprintf (stderr, WRITE_NONEXISTLIST);
		return NULL;
	}

	if (stream == stderr)
		fprintf (stream, "List %ld has %ld items in it.\n",
				this_list->list_count, this_list->occupancy);

	if (!this_list->write_func) {
		fprintf (stream, WRITE_MISSINGFUNC);
		return stream;
	}

	if (this_list->occupancy >= 1)
		working = this_list->end->next;

	for (count = 1; count <= this_list->occupancy; count++) {
		if (stream == stderr)
			fprintf (stream, "\nelement %ld:  ", count);
		write_Node (working, stream, this_list->write_func);
		working = working->next;
	}

	return stream;
}

/* Function:		write_reverse_List
 * Purpose:			writes element of list backwards
 * Description:		writes elements of list backwards starting with last item.
 *					the list is printed to filestream stream
 * Input:			pointer to the list, pointer to filestream
 * Result:			list is pointed out backwards, pointer to file returned */

FILE * write_reverse_List (List * this_list, FILE * stream) {

	/* decalre vars */
	long count;
	Node * working;

	/* if no filestream available */
	if (!stream) {
		fprintf (stderr, WRITE_NONEXISTFILE);
		return NULL;
	}

	/* if no list available */
	if (!this_list){
		fprintf(stderr, WRITE_NONEXISTLIST);
		return NULL;
	}

	/* if printeing to stderr */
	if (stream = stderr)
		fprintf (stream, "List %ld has %ld items in it.\n", 
				this_list->list_count, this_list->occupancy);

	/* if write_func exists */
	if (!this_list->write_func) {
		fprintf (stream, WRITE_MISSINGFUNC);
		return stream;
	}

	/* if list is size 1 or greater */
	if (this_list->occupancy >= 1)

		/* set working to end of list */
		working = this_list->end;

	/* loop through list to print  */
	for (count = this_list->occupancy; count >= 1; count--){

		/* print to stderr */
		if (stream == stderr)
			fprintf (stream, "\nelement %ld: ", count);

		/* calls write_Node, prints each element */
		write_Node (working, stream, this_list->write_func);

		/* sets working to previous node */
		working = working->pre;
	}

	return stream;
}


/* FUnction:		delete_Node
 * Purpose:			deletes Node in list
 * Description:		calls delete_func if it exists to delete a Node, frees
 *					the memory associated with the node, sets it ptr to null
 * Input:			double pointer to node, delete_func that returns void ptr
 * Results:			returns nothing, node is deleted 
 */

static void delete_Node (Node ** npp, void (*delete_func) (void *)) {

	/* does the node exist??? */
	if (!npp || !*npp) {
		fprintf (stderr, DELETE_NONEXISTNODE);
		return;
	}

	/* call function to delete element */
	if (delete_func && (*npp)->data)
		(*delete_func) (&((*npp)->data));

	/* delete element */
	free (*npp);

	/* assign node to NULL */
	*npp = NULL;
}


/* Function:		insert_Node
 * Purpose:			creates new node to hold element or copy of element
 * Description:		creates new node, then attaches it to its surrounding nodes 
 *					and attaches its surrounding nodes to it
 * Input:			pointer to this Node, the node after which we will insert
 *					void pointer to element, copy_func if it exists
 * Results:			returns a pointer to new node that is inserted
 */

static Node * insert_Node (Node * this_Node, void * element, 
		void * (*copy_func) (void *)) {

	/* initialises new Node inserted */
	Node * working;

	/* validity checks */


	working = new_Node (element, copy_func);

	/* to insert first node in list */
	if (this_Node == NULL){
		/* sets previous Node to working ????? */
		working -> pre = working;
		/* sets next Node to working ????? */
		working -> next = working;

		return working;
	}


	/* attach - sets up for next call of insert_Node */
	/* sets previous Node to Node after which new Node is inserted */
	working -> pre = this_Node;
	/* sets next Node to Node after after which new Node is inserted */
	working -> next = this_Node -> next;


	/* integrate */
	/* set this_Node's next to working */
	this_Node -> next = working;
	/* set this_Node's pre to working */
	working -> next -> pre = working;	

	return working;

}

/* Function:		new_Node
 * Purpose:			to create new node to be inserted into list
 * Descriptoin:		allocates memory for new node, then initalies the memory
 * Input:			void pointer to element to be inserted into new node
 *					copy_func
 * Results:			returns pointer to new node that's been inserted into list
 */

static Node* new_Node (void * element, void * (*copy_func) (void *)) {

	/* allocate memory */
	Node *this_Node = (Node *) malloc (sizeof (Node));

	/* initialize memory */
	this_Node->next = this_Node->pre = NULL;
	this_Node->data = (copy_func) ? (*copy_func) (element) : element;

	return this_Node;
}

/* Function:		remove_Node
 * Purpose:			removes singular node from list
 * Description:		unlinkes this node from list by arranging pointers of 
 *					surrounding nodes so they no longer point to this_node. 
 *					memory associated withe node object is freed, but node's 
 *					data is not deleted
 * Input:			pointer to this node
 * Results:			returns pointer to data
 */

static void * remove_Node (Node * this_Node) {

	/* if no node to remove */
	if (this_Node == NULL){
		fprintf(stderr, DELETE_NONEXISTNODE);
		return;
	}

	/* if there is a node to remove */
	else {

		/* if only 2 nodes in list, to remove one, set pre/next of remaining
		 * node to itself */
		if (this_Node->next == this_Node->pre) {
			this_Node->pre->pre = this_Node->pre;
			this_Node->pre->next = this_Node->next;
			this_Node->next = NULL;
			this_Node->pre = NULL;
		}

		/* if more than 2 nodes to remove, rearrange pointers accordingly */
		else {
			this_Node->pre->next = this_Node->next;
			this_Node->next->pre = this_Node->pre;
			this_Node->pre = NULL;
			this_Node->next = NULL;
		}
	}

	/* creates node pointer to data and sets it to data in node to be removed */
	Node * data = this_Node->data;	

	/* sets data in node to null */
	this_Node->data = NULL;

	/* calls delete_Node to delete memory associated with node and its data */
	delete_Node(&this_Node, NULL);

	/* return previously stored/saved data */
	return data;
}


/* Function:		view_Node
 * Purpose:			to view a node and its data
 * Descriptoin:		acceses data field of this node
 * Input:			pointer to this node
 * Results:			returns pointer to this node's data
 */

static void * view_Node (Node * this_Node) {

	/* if node doesn't exist */
	if (this_Node == NULL)
		fprintf(stderr, VIEW_NONEXISTNODE);

	/* return pointer to this node's data */
	return this_Node->data;
}

/* Function:		write_Node
 * Purpose:			to print data of current node to stream
 * Description:		prints node to stream
 * Input:			pointer to this node, pointer to filestream stream
 * Result:			pointer to stream returned
 */

static FILE* write_Node (Node * this_Node, FILE * stream,
		FILE * (*write_func) (void *, FILE *)) {

	if (!stream) {
		fprintf (stderr, WRITE_NONEXISTFILE);
		return NULL;
	}

	if (!this_Node) {
		fprintf (stream, WRITE_NONEXISTNODE);
		return stream;
	}

	if (!write_func) {
		fprintf (stream, WRITE_MISSINGFUNC);
		return stream;
	}

	return (*write_func) (this_Node->data, stream);
}
