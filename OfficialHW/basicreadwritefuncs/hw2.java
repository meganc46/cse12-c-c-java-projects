/**
 * The hw2 class is a direct port of hw2.c to java.
 * As you already know in java when you pass literal strings like
 * <P>
 *   writeline("a literal string\n", stream);
 * <P>
 * in C is considered a char[], but in java it's automatically
 * converted and treated as a String object.  Therefore 
 * the function writeline accepts literal strings and 
 * String types.  The getaline function returns a String type.
 */

import java.io.*;        // System.in and System.out
import java.util.*;      // Stack

class MyLibCharacter {
	private Character character;

	public MyLibCharacter (int ch) {
		character = new Character ( (char) ch );
	}

	public char charValue () {
		return character.charValue ();
	}

	public String toString () {
		return "" + character;
	}
}

public class hw2 {
	private static final int ASCII_ZERO = 48;
	private static final int COUNT = 16;		// # of hex digits

	private static final int CR = 13;		// Carriage Return
	private static final int MAXLENGTH = 80;	// Max string length

	private static final int EOF = -1;		// process End Of File

	private static final int DECIMAL = 10;		// to indicate base 10
	private static final int HEX = 16;		// to indicate base 16

	private static final char digits[] = 	// for ASCII conversion
		new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();

	private static final String DEBUG_GETALINE = 
		"[*DEBUG:  The length of the string just entered is ";

	private static final String DIGIT_STRING = "digit ";
	private static final String REENTER_NUMBER ="\nPlease reenter number: ";
	private static final String OUT_OF_RANGE = " out of range!!!\n";
	private static final String CAUSED_OVERFLOW = " caused overflow!!!\n";
	private static final String DEBUG_WRITELINE =
		"\n[*DEBUG:  The length of the string displayed is ";

	private static Stack<MyLibCharacter> InStream =
		new Stack<MyLibCharacter>();

	private static boolean debug_on = false;
	private static int hexCounter = 0; // counter for the number hex digits

	/**
	 * Takes in a positive number and displays in given base.
	 *
	 * @param	Numeric value to be displayed.
	 * @param	Base to use to display number.
	 * @param	Where to display, likely System.out or System.err.
	 */

	private static void baseout (int number, int base, PrintStream stream){
		int remainder;
		int index = 0; //counter for putting chars into arrays
		int i;
		int iterator;
		char [] Array = new char[COUNT];

		//to pad hexadeimcal with 0
		if(base != DECIMAL){
			for(i = 0; i<COUNT; i++){
				Array[i] = '0';
			}
		}

		//converts number into inputted base
		do {
			remainder = number % base;
			//converts number and puts it in array at specified
			//index, then increment index
			Array[index] = digits[remainder];
			index++;
			number = number/base;
		} while (number != 0);

		//prints out array backwards
		for (iterator = COUNT-1; iterator >= 0; iterator--){
			fputc(Array[iterator], stream);
		}
	}

	/**
	 * Function:	clrbuf
	 * Purpose:	when called, this fxn will clear stdin
	 * Descriptin:	checks to see if incoming parameter is already '\n', 
	 *		indicating that stdin is already clear; if it's already
	 *		clear, this function does nothing, otherwise it calls
	 *		fgec in a loop until stdin is clear
	 * Input:	character - the most recent character receieved from
	 *		previous call to fgetc
	 * Result:	stdin cleared, nothing returned
	 */

	public static void clrbuf (int character) {
		// YOUR CODE GOES HERE
		// while input is not newline, call fgetc to clear buffer 
		while ((char)character != '\n'){
			fgetc(System.in);
			System.out.println("buffering");
		}
	}

	/**
	 * Function:	decin
	 * Purpose:	accepts integer input from user
	 * Description:	processes user input in loop that ends when uesr enters
	 *		either valid number or EOF, if EOF is entered, EOF is
	 *		returned, otherwise ecah character entered is checked
	 *		to verify that it is numeric; non-numeric input is
	 *		identified, user is notified, reprompted, and loop
	 *		begins again; once input is verified to be valid, a
	 *		series of multiplicaiton by 10 and addition can take
	 *		place to convert ascii characters entered into numeric
	 *		quantity
	 * Input:	none
	 * Results:	number entered or EOF
	 */

	public static int decin() {
		// YOUR CODE GOES HERE
		// initialise variables
		int character = fgetc(System.in);
		int sum = 0;
		//loop through input
		while (character != '\n'){
			character = fgetc(System.in);
			int num = character - '0';
			//check if num >= 0 and <= 9
			if (num >= 0 && num <= 9){
				num = (sum)*(DECIMAL);
				sum = num + character;
				if (sum != num){
					digiterror (character,REENTER_NUMBER);
				}
			//check if overflow (if current sum / 10 is 
			//equal to sum of previous loop)
			}
			else if (character == EOF){
				return EOF;
			}
			else {
				//digiterror
			}
		}
		return (int)sum;
	}

	/**
	 * Takes in positive number and dispalys it in decimal.
	 * @param	positive numeric value to be displayed.
	 * @param	where to display, stdout or stderr
	 */

	public static void decout (int number, PrintStream stream){

		baseout(number, DECIMAL, stream);
	}


	/**
	 * Function Name:       digiterror
	 * Purpose:             This function handles erroneous user input
	 * Description:         This function displays an error message to user,
	 *			and asks for fresh input
	 * Input:		character:   character that began the problem
	 *			message:  The message to display to the user
	 * Result:              The message is displayed to the user
	 *			The result in progress needs to be set to 0 in
	 *			decin after the call to digiterror
	 */

	public static void digiterror (int character, String message) {

		/* handle error */
		clrbuf (character);

		/* output error message */
		writeline (DIGIT_STRING, System.err);
		fputc ( (char)character, System.err);
		writeline (message, System.err);

		writeline (REENTER_NUMBER, System.err);
	}

	/**
	 * Function:	getaline
	 * Purpose:	read a string from user
	 * description:	gets input from user via calls to fgetc up to some max
	 *		number of chracters; input is terminated when either
	 *		the max number of characters are entered or a newline
	 *		character is detected; if user enters more characters
	 *		than max, clrbuf is called to remove extra characters
	 *		which are ignored; if eof is detected, eof is passed
	 *		back to main program
	 * Input:	message - destination array where input is stored
	 *		maxlength - max number of nonNull characters allowed in
	 *		string + 1 for NULL character
	 * Result:	user input is stored in message, eof is returned when
	 *		user enters ^D; otherwise length of message returned
	 */

	public static int getaline( char message[], int maxlength ) {
		// YOUR CODE GOES HERE

		//initalise variables
		int i = 0;
		int input2;
		char input = 0;
		//loop for reading in input from system.in
		for (; i < maxlength; i++){
			input2 = fgetc(System.in);
			input = (char)input2;
			//check of EOF
			if (input2 == EOF){
				return EOF;
			}
			//if input is not newline, put into message
			//array
			else if (input != '\n'){
				message[i] = input;
			}
			//if input is neline, insert null character
			//at end and return length of input
			else {
				message[i] = '\0';
				return i;
			}
		}
		//if length of input is greater than maxlength then
		//clear buffer
		if (i > maxlength) {
			//clrbuf(input);
		}
		//debug statment
		if (debug_on == true){
			System.err.println(DEBUG_GETALINE + i + ".]");
		}
		//return length of input
		return i ;

	}

	/**
	 * Takes in a positive number and displays it in hex.
	 * @param	A positive numeric value to be diplyaed in hex.
	 * @param	Where to display, stdout or stderr
	 */

	public static void hexout(int number, PrintStream stream){
		//output '0x' for hexadecimal.
		writeline ("0x", stream);
		//calls baseout to convert to hexadecimal
		baseout (number, HEX, stream);
	}

	/**
	 * Returns a character from the input stream.
	 *
	 * @return  <code>char</code> 
	 */
	public static int fgetc(InputStream stream) {

		char ToRet = '\0';

		// Check our local input stream first.
		//   If it's empty read from System.in
		if (InStream.isEmpty ()) {

			try {
				// Java likes giving the user the
				// CR character too. Dumb, so just 
				// ignore it and read the next character
				// which should be the '\n'.                  
				ToRet = (char) stream.read ();
				if (ToRet == CR)
					ToRet = (char) stream.read ();

				// check for EOF
				if ((int) ToRet == 0xFFFF)
					return EOF;
			}

			// Catch any errors in IO.
			catch (EOFException eof) {

				// Throw EOF back to caller to handle
				return EOF;
			}

			catch (IOException ioe) {

				writeline ("Unexpected IO Exception caught!\n",
						System.out);
				writeline (ioe.toString (), System.out);
			}

		}

		// Else just pop it from the InStream.
		else
			ToRet = ((MyLibCharacter) InStream.pop ()).charValue ();
		return ToRet;
	}


	/**
	 * Displays a single character.
	 *
	 * @param    Character to display.
	 */
	public static void fputc(char CharToDisp, PrintStream stream) {

		// Print a single character.
		stream.print (CharToDisp);   

		// Flush the system.out buffer, now. 
		stream.flush ();
	}

	/**
	 * Prints out a newline character.
	 * @param	where to display, likely stdout or stderr.
	 */

	public static void newline (PrintStream stream){
		fputc('\n', stream);
	}

	/**
	 * Prints out  a string.
	 * @param	A string to print out
	 * @param	Where to display, likely stdout or stderr
	 * @return	<code>int</code> the length of the string.
	 */

	public static int writeline (String message, PrintStream stream){
		int index = 0;//counter to print char

		//converts string to array
		char[] messageArray = message.toCharArray();
		while (messageArray != null && index <= messageArray.length-1){
			fputc(message.charAt(index), stream);
			index++;
		}
		//debug message
		if (debug_on == true){
			System.err.println(DEBUG_WRITELINE + message.length());
		}
		// return size of array
		return message.length();
	}

	/**
	 * Places back a character into the input stream buffer.
	 *
	 * @param    A character to putback into the input buffer stream.
	 */
	public static void ungetc (int ToPutBack) {

		// Push the char back on our local input stream buffer.
		InStream.push (new MyLibCharacter (ToPutBack));
	}


	public static void main( String[] args ) {

		int base;                    /* to hold output base */
		char buffer[] = new char[MAXLENGTH];       /* to hold string */

		int number;                  /* to hold number entered */
		int strlen;                  /* length of string */

		/* initialize debug states */
		debug_on = false;

		/* check command line options for debug display */
		for (int index = 0; index < args.length; ++index) {
			if (args[index].equals("-x"))
				debug_on = true;
		} 

		/* infinite loop until user enters ^D */
		while (true) {
			writeline ("\nPlease enter a string:  ", System.out);

			strlen = getaline (buffer, MAXLENGTH);
			newline (System.out);

			/* check for end of input */
			if ( EOF == strlen )
				break;

			writeline ("The string is:  ", System.out);
			writeline ( new String(buffer), System.out);

			writeline ("\nIts length is ", System.out);
			decout (strlen, System.out);
			newline (System.out);

			writeline ("\nPlease enter a decimal number:  ",
					System.out);
			if ((number = decin ()) == EOF)
				break;

			writeline ("\nPlease enter an output base (2-36):  ",
					System.out);
			if ((base = decin ()) == EOF)
				break;

			writeline ("Number entered is:  ", System.out);
			decout (number, System.out);

			writeline ("\nAnd in hexidecimal is:  ", System.out);
			hexout (number, System.out);

			writeline ("\nAnd in base ", System.out);
			decout (base, System.out);
			writeline (" is:  ", System.out);
			baseout (number, base, System.out);

			writeline ("\nNumber entered multiplied by 8 is:  ", System.out);
			decout (number << 3, System.out);
			writeline ("\nAnd in hexidecimal is:  ", System.out);
			hexout (number << 3, System.out);

			newline (System.out);
		}
	}
}
