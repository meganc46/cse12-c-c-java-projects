/****************************************************************************

  Megan Chang
  CSE 12, Fall 2015
  Oct 7, 2015
  cs12xee
  Assignment Two

  File Name:      hw2.c
Description:    This program reads strings and integers from the user,
processes them, and prints them back to the user.  Program
terminates when user enters ^D.  At termination, program
outputs sizes of various types of C/C++ pre defined types.

 ****************************************************************************/

/* declare fputc/fgetc */
#include <stdio.h>
#include <getopt.h>

/* define some program-wide constants */
#define ASCII_ZERO '0'
#define COUNT ((long) (sizeof (long) << 1))
#define ERROR -1
#define FALSE 0
#define MAXLENGTH 8
#define OFFSET ('a' - 'A')
#define SPACE ' '
#define TRUE 1

#define DECIMAL 10
#define HEX 16

/* define the keyword "NULL" as 0 */
#ifdef NULL
#undef NULL
#endif
#define NULL 0

/* declarations for functions defined in this file */
void baseout (long number, long base, FILE *stream);
void clrbuf (int);
long decin (void);
void decout (unsigned long, FILE *);
void digiterror (int, long *, const char *);
long getaline (char *, long);
void hexout (unsigned long, FILE *);
void newline (FILE *);
long writeline (const char *, FILE *);

/* array for input checking and for output */
const char digits[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

/* messages */
const char CAUSED_OVERFLOW[] =  " caused overflow!!!\n";
const char DIGIT_STRING[] = "digit ";
const char REENTER_NUMBER[] = "\nPlease reenter number: ";
const char OUT_OF_RANGE[] = " out of range!!!\n";

/* debug messages */
const char DEBUG_GETALINE[] =
"[*DEBUG:  The length of the string just entered is ";
const char DEBUG_WRITELINE[] =
"\n[*DEBUG:  The length of the string displayed is ";
static long debug_on = FALSE;

/*--------------------------------------------------------------------------
  Function Name:	baseout
Purpose:		Converts inputted integer number to passed in base
Description:		Seperate inputted number into individual character, 
convert each character into individual value, insert
it in array at index, increment index; after all 
characters are in array, print out array backwards.
Input:		number - positive integer inputted by user
base - positive integer inputted by user
 * stream - stdout or stderr
Result:		Result of function is number converted into specified;
nothing returned bu converted number is displayed
--------------------------------------------------------------------------*/
void baseout (long number, long base, FILE * stream) {
	int remainder;
	int index = 0; /*counter for putting chars into arrays*/
	int i;
	int iterator;
	char Array[BUFSIZ];

	/* to pad hexadecimal with 0*/
	if (base != DECIMAL) {
		for (i = 0; i < COUNT; i++){
			Array[i]= '0';
		}
	}

	/* converts number into inputted base */
	do {
		remainder = number % base;
		/* converts number and puts it in array at specified index,
		 * then increments index
		 */
		Array[index] = digits[remainder];
		index++;
		number = number/base;
	} while (number !=0);
	if (base != DECIMAL){
		index = COUNT;
	}
	/*prints out array backwards*/
	for (iterator = index-1; iterator >= 0; iterator--){
		fputc(Array[iterator], stream);
	}
}


/*-------------------------------------------------------------------------- 
  Function Name:        clrbuf 
Purpose:              When called, this function will clear stdin. 
Description:          This function checks to see if the incoming 
parameter is already '\n' indicating that stdin 
is already clear.  If stdin is already clear, this 
function does nothing.  Otherwise, this function 
calls "fgetc" in a loop until stdin is clear. 
Input:                character:  the most recent character received from a 
previous call to fgetc. 
Result:               stdin cleared.  Nothing returned. 
--------------------------------------------------------------------------*/  
void clrbuf (int character) {  
	/* YOUR CODE GOES HERE */ 
	/* while input is not newline, call fgetc to clear buffer */
	while ((char)character != '\n'){
		fgetc(stdin);
		printf("buffering");
	}
}


/*--------------------------------------------------------------------------
  Function Name:        decin
Purpose:              This function accepts integer input from from the user.
Description:          This function processes user input in a loop that ends
when the user enters either a valid number or EOF.
If EOF is entered, EOF is returned.  Otherwise each
character entered is checked to verify that it is
numeric.  Non-numeric input is identified, the user
is notified, reprompted, and the loop begins again.
Once the input is verified to be valid, a series
of multiplication by 10 and addition can take
place to convert the ASCII characters entered into
a numeric quantity.
Input:                None.
Result:               The number entered or EOF.
--------------------------------------------------------------------------*/
long decin (void) {
	/* YOUR CODE GOES HERE */
	/* initialise variables */
	int character = fgetc(stdin);
	long sum = 0;
	int num;
	/* loop through input */
	while (character != '\n'){
		character = fgetc(stdin);
		num = character - '0';
		/* check if input is valid pos integer */
		if (num >= 0 && num <= 9){
			num = (sum)*(DECIMAL);
			sum = num + character - '0';
			/* check if overflow (if current sum/10 is equal to 
			 * sum in previous loop iteration */

			if (num != (sum/10)){
				/*digiterror(character,sum *, REENTER_NUMBER);*/
			}

		}
		else {
			/* digiterror(character, sum *, REENTER_NUMBER);*/
		}

	}
	return sum;
}


/*--------------------------------------------------------------------------
  Function name:		decout
Purpose:		Displays inputted integer number to filestream as
integer
Description:		Calls baseout
Input:			number - any positive integer
 * stream - stdout or stderr
Result:			Number in base 10 is displayed - nothing returned
--------------------------------------------------------------------------*/
void decout (unsigned long number, FILE * stream) {
	baseout(number, DECIMAL, stream);
}


/*--------------------------------------------------------------------------
  Function Name:          digiterror
Purpose:                This function handles erroneous user input.
Description:            This function reinitializes sum, displays and
error message to the user, and asks for fresh
input.
Input:                  character:  The character that began the problem.
sum:  A pointer to the sum to reinitialize.
message:  The message to display to the user.
Result:                 The message is displayed to the user.  sum is 0.
--------------------------------------------------------------------------*/
void digiterror (int character, long * sum, const char * message) {

	/* handle error */
	clrbuf (character);

	/* reset sum */
	*sum = 0;

	/* output error message */
	writeline (DIGIT_STRING, stderr);
	fputc (character, stderr);
	writeline (message, stderr);

	writeline (REENTER_NUMBER, stdout);
}


/*--------------------------------------------------------------------------
  Function Name:          getaline
Purpose:                This function will read a string from the user.
Description:            This function gets input from the user via
calls to fgetc up to some maximum number of
characters.  Input is terminated when either the
maximum number of characters are entered, or
a newline character is detected.  If the user
enters more characters than the maximum, clrbuf
is called to remove extra characters which are
ignored.  Since this is routine accepts input,
if EOF is detected EOF is passed back to the main
program.
Input:                  message:  the destination array where input is stored.
maxlength:  the maximum number of non-NULL characters
allowed in the string + 1 for the NULL char.
Result:                 User input is stored in message.
EOF is returned when the user enters ^D.
Otherwise, the length of the message is returned.
--------------------------------------------------------------------------*/
long getaline (char * message, long maxlength) {
	/* YOUR CODE GOES HERE */
	int i = 0;
	/*while (fgetc(stdin) != '\n'){*/
	for (;i<maxlength;i++){
		int input2 = fgetc(stdin);
		char input = (char)input2;
		if (input2 == EOF){
			return EOF;
		}
		else if (input != '\n'){
			message[i] = input;
		}
		else {
			message[i] = '\0';
			return i;
		}
	}
	/* if length of input is greater than maxlength, clear buffer */
	if (i > maxlength) {
		/* clrbuf(input); */
	}
	/* debug statement */
	if (debug_on ==1){
		fprintf(stderr, DEBUG_GETALINE + i);
	}

	/* return length of input */
	return 0;
}


/*--------------------------------------------------------------------------
Function:		hexout
Purpose:		Prints a number in base 16 to the parameter FILE stream
Description:		Goal is achieved via delegating to baseout function
Input:			number - the number to display
stream - where to display, likely stdout or stderr
Results:		Number in base 16 is displayed, no return value
--------------------------------------------------------------------------*/
void hexout (unsigned long number, FILE * stream){
	/* Output "0x" for hexadecimal*/
	long writeline (const char * message, FILE * stream);
	writeline ("0x", stream);
	baseout (number, HEX, stream);
}

/*--------------------------------------------------------------------------
Function:		newline
Purpose:		Displays newline character ('\n') to filestream
Description:		Displays using fputc function
Input:			stream - stdout or stderr
Result:			Newline character is displayed, nothing returned
--------------------------------------------------------------------------*/
void newline (FILE * stream){
	fputc('\n', stream);
}

/*--------------------------------------------------------------------------
Function:		writeline
Purpose:		displays string message via filestream
Description:		uses loop to call fputc, which displaysmessage to stream
Input:			message - null terminated character arrays
stream - stdout or stderr
Results:		string message is displayed to user, integer returned
--------------------------------------------------------------------------*/
long writeline (const char * message, FILE * stream){
	int index = 0;
	/* while input isn't null character, print out input */
	while (message[index] != '\0') {
		fputc(message[index], stream);
		index++;
	}
	/* return length of input */
	return sizeof(message);
}

/*--------------------------------------------------------------------------
  Function Name:          main
Description:            This function asks for input and displays output
Purpose:                This program asks the user to enter a string
and a number.  Computations are performed on the 
strings and numbers, and the results are displayed.
Description:            This function asks for input and displays output
in an infinite loop until EOF is detected.  Once EOF
is detected, the lengths of the types are displayed.
Input:                  None.
--------------------------------------------------------------------------*/
int main (int argc, char *const* argv) {

	long base;                  /* to hold output base */
	char buffer[MAXLENGTH];     /* to hold string */
	long number;                /* to hold number entered */
	long strlen;                /* length of string */

	long array[10];             /* to show user where memory is allocated */
	long * ap = array;	    /* to show user about addresses in memory */
	long ** app = &ap;	    /* to show user about addresses in memory */
	long * apx = &array[0];	    /* to show user about addresses in memory */
	char option;                /* the command line option */

	/* initialize debug states */
	debug_on = FALSE;

	/* check command line options for debug display */
	while ((option = getopt (argc, argv, "x")) != EOF) {
		switch (option) {
			case 'x': debug_on = TRUE; break;
		}
	}

	/* infinite loop until user enters ^D */
	while (1) {
		writeline ("\nPlease enter a string:  ", stdout);
		strlen = getaline (buffer, MAXLENGTH);
		newline (stdout);

		/* check for end of input */
		if (strlen == EOF)
			break;

		writeline ("The string is:  ", stdout);
		writeline (buffer, stdout);

		writeline ("\nIts length is ", stdout);
		decout (strlen, stdout);
		newline (stdout);

		writeline ("\nPlease enter a decimal number:  ", stdout);
		if ((number = decin ()) == EOF)
			break;

		writeline ("\nPlease enter an output base (2-36):  ", stdout);
		if ((base = decin ()) == EOF)
			break;

		writeline ("Number entered is:  ", stdout);
		decout (number, stdout);

		writeline ("\nAnd in hexidecimal is:  ", stdout);
		hexout (number, stdout);

		writeline ("\nAnd in base ", stdout);
		decout (base, stdout);
		writeline (" is:  ", stdout);
		baseout (number, base, stdout);

		writeline ("\nNumber entered multiplied by 8 is:  ", stdout);
		decout (number << 3, stdout);
		writeline ("\nAnd in hexidecimal is:  ", stdout);
		hexout (number << 3, stdout);

		newline (stdout);
	}

	writeline ("\nThe value of ap is:  ", stdout);
	decout ((long) ap, stdout);
	writeline ("\nAnd in hexidecimal is:  ", stdout);
	hexout ((long) ap, stdout);
	newline (stdout);

	writeline ("The value of app is:  ", stdout);
	decout ((long) app, stdout);
	writeline ("\nAnd in hexidecimal is:  ", stdout);
	hexout ((long) app, stdout);
	newline (stdout);

	writeline ("The value of apx is:  ", stdout);
	decout ((long) apx, stdout);
	writeline ("\nAnd in hexidecimal is:  ", stdout);
	hexout ((long) apx, stdout);
	newline (stdout);

	writeline ("The value of ap + 1 is:  ", stdout);
	decout ((long) (ap+1), stdout);
	writeline ("\nAnd in hexidecimal is:  ", stdout);
	hexout ((long) (ap+1), stdout);
	newline (stdout);

	writeline ("The address of array[0] is:  ", stdout);
	decout ((long) &array[0], stdout);
	newline (stdout);

	writeline ("The address of array[1] is:  ", stdout);
	decout ((long) &array[1], stdout);
	newline (stdout);

	writeline ("The size of a float is:  ", stdout);
	decout (sizeof (float), stdout);
	newline (stdout);

	writeline ("The size of a double is:  ", stdout);
	decout (sizeof (double), stdout);
	newline (stdout);

	writeline ("The size of a long double is:  ", stdout);
	decout (sizeof (long double), stdout);
	newline (stdout);

	writeline ("The size of a char is:  ", stdout);
	decout (sizeof (char), stdout);
	newline (stdout);

	writeline ("The size of an int is:  ", stdout);
	decout (sizeof (int), stdout);
	newline (stdout);

	writeline ("The size of a short is:  ", stdout);
	decout (sizeof (short), stdout);
	newline (stdout);

	writeline ("The size of a short int is:  ", stdout);
	decout (sizeof (short int), stdout);
	newline (stdout);

	writeline ("The size of a long is:  ", stdout);
	decout (sizeof (long), stdout);
	newline (stdout);

	writeline ("The size of a long int is:  ", stdout);
	decout (sizeof (long int), stdout);
	newline (stdout);

	writeline ("The size of a long long is:  ", stdout);
	decout (sizeof (long long), stdout);
	newline (stdout);

	writeline ("The size of a signed is:  ", stdout);
	decout (sizeof (signed), stdout);
	newline (stdout);

	writeline ("The size of a signed char is:  ", stdout);
	decout (sizeof (signed char), stdout);
	newline (stdout);

	writeline ("The size of a signed short is:  ", stdout);
	decout (sizeof (signed short), stdout);
	newline (stdout);

	writeline ("The size of a signed short int is:  ", stdout);
	decout (sizeof (signed short int), stdout);
	newline (stdout);

	writeline ("The size of a signed int is:  ", stdout);
	decout (sizeof (signed int), stdout);
	newline (stdout);

	writeline ("The size of a signed long is:  ", stdout);
	decout (sizeof (signed long), stdout);
	newline (stdout);

	writeline ("The size of a signed long int is:  ", stdout);
	decout (sizeof (signed long int), stdout);
	newline (stdout);

	writeline ("The size of a signed long long is:  ", stdout);
	decout (sizeof (signed long long), stdout);
	newline (stdout);

	writeline ("The size of an unsigned is:  ", stdout);
	decout (sizeof (unsigned), stdout);
	newline (stdout);

	writeline ("The size of an unsigned char is:  ", stdout);
	decout (sizeof (unsigned char), stdout);
	newline (stdout);

	writeline ("The size of an unsigned short is:  ", stdout);
	decout (sizeof (unsigned short), stdout);
	newline (stdout);

	writeline ("The size of an unsigned short int is:  ", stdout);
	decout (sizeof (unsigned short int), stdout);
	newline (stdout);

	writeline ("The size of an unsigned int is:  ", stdout);
	decout (sizeof (unsigned int), stdout);
	newline (stdout);

	writeline ("The size of an unsigned long is:  ", stdout);
	decout (sizeof (unsigned long), stdout);
	newline (stdout);

	writeline ("The size of an unsigned long int is:  ", stdout);
	decout (sizeof (unsigned long int), stdout);
	newline (stdout);

	writeline ("The size of an unsigned long long is:  ", stdout);
	decout (sizeof (unsigned long long), stdout);
	newline (stdout);

	writeline ("The size of a void pointer is:  ", stdout);
	decout (sizeof (void *), stdout);
	newline (stdout);

	writeline ("The size of a character pointer is:  ", stdout);
	decout (sizeof (char *), stdout);
	newline (stdout);

	writeline ("The size of an int pointer is:  ", stdout);
	decout (sizeof (int *), stdout);
	newline (stdout);

	writeline ("The size of a long pointer is:  ", stdout);
	decout (sizeof (long *), stdout);
	newline (stdout);

	writeline ("The size of a float pointer is:  ", stdout);
	decout (sizeof (float *), stdout);
	newline (stdout);

	writeline ("The size of a double pointer is:  ", stdout);
	decout (sizeof (double *), stdout);
	newline (stdout);

	writeline ("The size of a long double pointer is:  ", stdout);
	decout (sizeof (long double *), stdout);
	newline (stdout);

	newline (stdout);

	return 0;
}
