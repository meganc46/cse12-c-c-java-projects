/******************************************************************************
 
 Megan Chang
 CSE 12, Fall 2015
 Oct 14, 2015
 cs12xee
 Assignment Three

 Filename:	stack.c
 Description:	this program creates and array-based stack of longs. it performs
		push, pop, top, empty functions on it, adhering to LIFO rule. 
		Program terminates when user enters ^D.

*******************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include "mylib.h"
#include "stack.h"

#define STACK_POINTER_INDEX (-1)        /* Index of last used space */
#define STACK_SIZE_INDEX (-2)           /* Index of size of the stack */
#define STACK_COUNT_INDEX (-3)          /* Index of which stack allocated */
#define STACK_OFFSET 3  /* offset from allocation to where user info begins */

/* catastrophic error messages */
static const char DELETE_NONEXIST[] = "Deleting a non-existent stack!!!\n";
static const char EMPTY_NONEXIST[] = "Emptying a non-existent stack!!!\n";
static const char INCOMING_NONEXIST[] = 
                        "Incoming parameter does not exist!!!\n";
static const char ISEMPTY_NONEXIST[] = 
                        "Isempty check from a non-existent stack!!!\n";
static const char ISFULL_NONEXIST[] = 
                        "Isfull check from a non-existent stack!!!\n";
static const char NUM_NONEXIST[] = 
                        "Num_elements check from a non-existent stack!!!\n";
static const char POP_NONEXIST[] = "Popping from a non-existent stack!!!\n";
static const char POP_EMPTY[] = "Popping from an empty stack!!!\n"; 
static const char PUSH_NONEXIST[] = "Pushing to a non-existent stack!!!\n";
static const char PUSH_FULL[] = "Pushing to a full stack!!!\n";
static const char TOP_NONEXIST[] = "Topping from a non-existent stack!!!\n";
static const char TOP_EMPTY[] = "Topping from an empty stack!!!\n";
static const char WRITE_NONEXIST_FILE[] = 
                        "Attempt to write using non-existent file pointer!!!\n";
static const char WRITE_NONEXIST_STACK[] = 
                        "Attempt to write to a non-existent stack!!!\n";

/* Debug messages. HEX messages used for negative numbers on the stack. */
static const char ALLOCATED[] = "[Stack %ld has been allocated]\n";
static const char DEALLOCATE[] = "[Stack %ld has been deallocated]\n";
static const char HEXPOP[] = "[Stack %ld - Popping 0x%lx]\n";
static const char HEXPUSH[] = "[Stack %ld - Pushing 0x%lx]\n";
static const char HEXTOP[] = "[Stack %ld - Topping 0x%lx]\n";
static const char POP[] = "[Stack %ld - Popping %ld]\n";
static const char PUSH[] = "[Stack %ld - Pushing %ld]\n";
static const char TOP[] = "[Stack %ld - Topping %ld]\n";

/* static variable allocation */
static int debug = FALSE; /* allocation of debug flag */
static int stack_counter = 0; /* number of stacks allocated so far */


/* Debug state methods */
void debug_off (void) {
        debug = FALSE;
}

void debug_on (void) {
        debug = TRUE;
}
 
/* start of true stack code */

/* Function:	delete_Stack
 * Purpose:	destructor function that deallocates all memory associated 
 *		with the stack and sets its pointer in the calling function 
 *		to NULL.
 * Description:	checks if parameter is empty; if not, frees memory used by 
 *		this_Stack and sets pointer to stack to null, then decrements
 *		number of stacks allocated
 * Input:	spp - pointer to pointer to this_Stack
 * Results:	memory associated with stack is deallocated, no return value
 */

void delete_Stack (Stack ** spp) {
    /* checks if parameter is null, if so prints error msg */
    if ((spp == NULL) | (*spp == NULL)){
	if (debug)
	    fprintf(stderr, DELETE_NONEXIST);
    }
    /* if parameter exists */
    else{
	/* fre memory associated with stack */
	free(*spp - STACK_OFFSET);
	/* set dereferenced pointer to null */
	*spp = NULL;
	/*debug stmt */
	if (debug)
	    fprintf(stderr, DEALLOCATE, STACK_COUNT_INDEX);
	/* decrements number of stacks allocated */
	stack_counter--;
    }
}

/* Function:	empty_Stack
 * Purpose:	empties stack of all its elements
 * Description: check that stack exists,  if stack exists and  isn't already 
 *		empty, empty it by setting the last used index to -1, and 
 *		reset stack size
 * Input:	this_Stack - stack to be emptied
 * REsults:	stack is emptied of all elements, no return value
 */

void empty_Stack (Stack * this_Stack) {
    /* checks if stack exists */
    if (this_Stack == NULL){
	if (debug)
		fprintf (stderr, EMPTY_NONEXIST);
    }
    /* if stack isn't already empty, empty it */
    if (!isempty_Stack(this_Stack)){
	/* sets last used index to -1, thus muting all subsequent elemnts
	 * and effectively emptying the stack 
	 */
	this_Stack[STACK_POINTER_INDEX] = -1;
	/* sets size of stack used to 0 */
	this_Stack[STACK_SIZE_INDEX] = 0;
    }
}

/* Function:	isempty_Stack
 * Purpose:	checks is stack is empty
 * Description:	returns true is stack is empty, false if it is not
 * Input:	this_stack - stack to be checked for emptiness
 * Result:	either true or false i(0/1) returned, signifying if stack empty
 */

long isempty_Stack (Stack * this_Stack) {
    /* checks is stack exists */
    if (this_Stack == NULL){
	if (debug)
	    fprintf(stderr, ISEMPTY_NONEXIST);
    }
    /* if last used index is 0, then stack is empty and return true */
    else if (this_Stack[STACK_POINTER_INDEX] < 0 ){
	return 1;
    }
    /* return false if stack is not empty */
    else {
	return 0;
    }
}

/* Function:	isfull_Stack
 * Purpose:	checks if stack is full
 * Descrption:	returns true if stack is full, false if it is not
 * Input:	this_Stack - stack to be checked for fullness
 * Result:	either true or false (0/1) returned, signifying if stack is full
 */

long isfull_Stack (Stack * this_Stack) {
    /* checks if stack exists */
    if (this_Stack == NULL){
	if (debug)
	    fprintf(stderr, ISFULL_NONEXIST);
    }
    /* returns true if last used index equals total size of used indexes */
    else if (this_Stack[STACK_POINTER_INDEX]==this_Stack[STACK_SIZE_INDEX]){
	return 1;
    }
    /* returns false if stack is not full */
    else {
	return 0;
    }
}

/* Function:	new_Stack
 * Purpose:	to initialise new stack object and allocate memory for new 
 *		stack in heap
 * Description:	allocates memory using malloc  to hold stacksize number of longs
 *		, initialises stack infrasturcutre, and returns pointer to 
 *		first storage space in stack
 * Input:	stacksize = unsigned long value reps size of stack allocated
 * Results:	new stack object initialised and allocated emory for in inheap
 */

Stack * new_Stack (unsigned long stacksize) {
    /* assigns memory to void pointer, using malloc to allocated memory, taking
     * into account offset amount and size of indiviaudel long 
     */
    void * memory = malloc((STACK_OFFSET + stacksize)*sizeof(long));
    /* create pointer to malloc, initialising it to malloc and offset value */
    Stack * this_Stack = (Stack*)memory + STACK_OFFSET;
    /* sets last used index in stack to -1 */
    this_Stack[STACK_POINTER_INDEX] = -1;
    /* sets stacksize to total size of indexes used */
    this_Stack[STACK_SIZE_INDEX] = stacksize;
    /* increments number of stacks allocated thus far */
    stack_counter++;
    /* sets value of stack_counter to static var already keeping track of it */
    this_Stack[STACK_COUNT_INDEX] = stack_counter;
    if (debug)
	fprintf(stderr, ALLOCATED, stack_counter);
    /* returns newly created stack */
    return this_Stack;
}

/* Function:	num_elements
 * Purpose:	returns number of elements in stack
 * Description: returns index of last used pionter + 1 in stack
 * Input:	this_Stack - stack from which num elements to be retreived from
 * Rsults:	returns number of elements in stack
 */

long num_elements (Stack * this_Stack) {
    /* checks if stack exists */
    if (this_Stack == NULL){
	if (debug)
		fprintf(stderr, NUM_NONEXIST);
    }
    /* if last used index is less than total number of used elements...*/
    else if (this_Stack[STACK_POINTER_INDEX] < this_Stack[STACK_SIZE_INDEX]){
	/* return last used index + 1, which reps number of elements in stack */
	return this_Stack[STACK_POINTER_INDEX]+1;
    }
}

/* Function:	pop
 * Purpose:	removes item from top of stack, sending back through output
 *		parameter 'item'
 * Description:	pop last inputted item by decrementing last used index in stack
 *		effectively muting item from top of stacki, returning true/false
 *		depending whether item succesfully popped
 * Input:	this_Stack - stack element to be popped from
 *		item - element from stack to be popped
 * Result:	item from top of stack is popped, returns 0/false or 1/true
 */

long pop (Stack * this_Stack, long * item) {
    /* checks if stack exists */
    if (this_Stack == NULL){
	if (debug)
	    fprintf(stderr, POP_NONEXIST);
	return 0;
	}
    /* checks is stack is empty */
    if (isempty_Stack(this_Stack)){
	if (debug)
	    fprintf(stderr, POP_EMPTY);
    }
    /* if stack is not empty...*/
    else if (!isempty_Stack(this_Stack)){
	/* dereference item pointer, setting its value to last used index*/
	*item = this_Stack[this_Stack[STACK_POINTER_INDEX]];
	/* decrement last used index */
	this_Stack[STACK_POINTER_INDEX]--;
	if (debug)
	    fprintf(stderr, POP, stack_counter, *item);
	return 1;
    }
    else {
	return 0;
    }
}

/* Function:	push
 * Purpose:	add items to top of stack
 * Description:	adds item inputted by user to next available index, returning
 *		true if successul and false if not
 * Input:	this_Stack - stack to be pushed onto
 *		item - element to be pushed onto stack
 * Result:	new item is added to top of stack, return 1/true or 0/false
 */

long push (Stack * this_Stack, long item) {
    /* checks if stack exists */
    if (this_Stack == NULL){
	if (debug)
	    fprintf(stderr, PUSH_NONEXIST);
	return 0;
	}

    /* checks if stack is full, able to be pushed onto */
    if (isfull_Stack(this_Stack)){
	if (debug)
	    fprintf(stdout, PUSH_FULL);
	return 0;
    }
    /* if stack is not full..*/
    else{
	/* increment last used index in stack, go to newly incremented index
	 * position in stack, and assign that space to item
	 */
	this_Stack[++this_Stack[STACK_POINTER_INDEX]] = item;
	if (debug)
	    fprintf(stderr, PUSH, stack_counter, item);
	return 1;
    }
}

/* Function:	top
 * Purpose:	previews items on top of stack
 * Description:	sends back item on top of stack through output parameter, doesnt
 *		remove it from stack. returns true on success and false if not
 * Input:	this_Stack - stack to be topped from
 *		item - item to be topped
 * Result:	item on top of stack is previewed through output parameter
 *		0/false returned if not topped successfully (stack empty)
 *		1/true returned if successful
 */

long top (Stack * this_Stack, long * item) {
    /* checks if stack exists */
    if (this_Stack == NULL){
	if (debug)
	    fprintf(stderr, TOP_NONEXIST);
    return 0;
	}
    /* checks if stack is mepty */
    if (isempty_Stack(this_Stack)){
	if(debug)
	    fprintf(stderr, TOP_EMPTY);
    }
    /* if stack is not empty...*/
    else if (!isempty_Stack(this_Stack)){
	/* dereference item pointer, assigns value at last used pointer to it */
	*item = this_Stack[this_Stack[STACK_POINTER_INDEX]];
	if (debug)
	    fprintf(stderr, TOP, stack_counter, *item);
	return 1;
    }
    else {
	return 0;
    }
}

/* function:	write_Stack
 * purpose:	write stack to stderr or stdout
 * description:	writes stack to appropriate stream, prints out debug stmts
 * Input:	this_Stack - relevant stack with info to be written
 *		stream - stderr or stdout
 * Result:	stack elements printed out, debug msgs printed out
 *		pointer to file where write_stack is allocated created
 */

FILE * write_Stack (Stack * this_Stack, FILE * stream) {

        long index = 0;         /* index into the stack */

        if (this_Stack == NULL) {
                fprintf (stderr, WRITE_NONEXIST_STACK);
                return stream;
        }

        if (stream == NULL) {
                fprintf (stderr, WRITE_NONEXIST_FILE);
                return stream;
        }
                
        if (stream == stderr)
                fprintf (stream, "Stack has %ld items in it.\n",
                        num_elements (this_Stack));

        for (index = STACK_COUNT_INDEX + STACK_OFFSET;
                index < num_elements (this_Stack); index++) {

                if (stream == stderr)
                        fprintf (stream, "Value on stack is |0x%lx|\n",
                                this_Stack[index]);
                else {
                        if (this_Stack[index] < 0)
                                fprintf (stream, "%c ",
                                        (char) this_Stack[index]);
                        else
                                fprintf (stream, "%ld ", this_Stack[index]);
                }
        }

        return stream;
}
