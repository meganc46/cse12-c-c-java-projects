/***
 *
 * Megan Chang
 * CSE 12 Fall 2015
 * cs12xee
 * HW9
 * Dec 3 2015
 *
 * File: Driver.h
 * Description: Header file, contains method decalrations and
 * private/publici nfo
 */

#ifndef DRIVER_H
#define DRIVER_H

#include <string.h>
#include <iostream>
#include <cstdlib>
using namespace std;

// length of character name array
const int length = 16;

class UCSDStudent {
        friend ostream & operator << (ostream &, const UCSDStudent &);
        char name[16];
        long studentnum;
public:
	
	/* 
	 * Function: Constructor w/ two arguments
	 * Parameters: nm (character array of the name)
	 *			   stunum (number fo assign to student)
	 */

	UCSDStudent (char * nm, long stunum) : studentnum (stunum) {
		memset (name, '\0', sizeof (name));
		strcpy(name, nm);
	}
	
	/* Function: Destructor for UCSD Student
	 * Parameters: none
	 */

	~UCSDStudent(void) {}

	/* 
	 * Function:  Default constructor 
	 * Parameters: none
	 */

	UCSDStudent(void) : studentnum(0) {
		memset (name, '\0', sizeof (name));
	}
	
	/* 
	 * UCSDStudent copy constructor 
	 * Paratmers: ucsd student object
	 */

	UCSDStudent (const UCSDStudent & student) {
		memset (name, '\0', sizeof (name));
		strcpy (name, student.name);
		studentnum = student.studentnum;
	}
	
	/*
	 * Overrides operator cast to const char * to return name
	 * Paramters: none
	 * Return: studnet name
	 */

	operator const char * (void) const {
		return name;
	}
	
	/*
	 * Overrides operator ==, compares students names
	 * Parameters: stu (UCSDStudent object compared)
	 * Return:	nonzero/true if equal, 0/false otherwise
	 */

	long operator == (const UCSDStudent & student) const {
		return ! strcmp (name, student.name);
	}

	/*
	 * Overrides operator <, compares student names
	 * Paramters:	stu (UCSDSTudent object compared)
	 * Return:	nonzer/true if equal, 0/false otherwise
	 */

	long operator < (const UCSDStudent & student) const {
		return (strcmp(name, student.name) < 0) ? 1 : 0;
	}
};

#endif
