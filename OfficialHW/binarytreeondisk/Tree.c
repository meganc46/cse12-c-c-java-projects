/*********************
 *
 * Megan Chang
 * CSE 12 Fall 2015
 * Assignment Nine
 * cs12xee
 * Dec 3 2015
 *
 * Filename: Tree.c
 * Description: To implement a binary tree similar to that in hw8, but
 * binary tree is implemented as a binary tree on disk. The user can
 * insert, lookup, delete at UCSDStudent in the tree.
 *
 *********************/

#include <stdlib.h>
#include <string.h>
#include "Tree.h"

// debug messages
static const char ALLOCATE[] = " - Allocating]\n";
static const char COST_READ[] = "[Cost Increment (Disk Access): Reading ";
static const char COST_WRITE[] = "[Cost Increment (Disk Access): Writing ";
static const char DEALLOCATE[] = " - Deallocating]\n";
static const char TREE[] = "[Tree ";

template <class Whatever>
int Tree<Whatever>::debug_on = 0;

template <class Whatever>
long Tree<Whatever>::cost = 0;

template <class Whatever>
long Tree<Whatever>::operation = 0;

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define THRESHOLD 2

template <class Whatever>
ostream & operator << (ostream &, const TNode<Whatever> &);

template <class Whatever>
struct  TNode {
// friends:

// data fields:
	Whatever data;
	long height;
	long balance;
	offset left;
	offset right;
	offset this_position;	// current position

// function fields:
	TNode () : height (0), balance (0), left (0), right (0), 
		this_position (0) {}

	// to declare the working TNode in Tree's Remove
	TNode (Whatever & element) : data (element), height (0), balance (0),
		left (0), right (0), this_position (0) {}
	
	TNode (Whatever &, fstream *, long &);	// to add new node to disk
	TNode (const offset &, fstream *);	// to read node from disk
	
	unsigned long Insert (Whatever &, fstream *, long &, offset &);
	// optional recursive Lookup declaration 
	unsigned long Lookup (Whatever &, fstream *) const;
	void Read (const offset &, fstream *);	// read node from disk
	unsigned long Remove (TNode<Whatever> &, fstream *, long &, offset &,
		long fromSHB = FALSE);
	void ReplaceAndRemoveMin (TNode<Whatever> &, fstream *, offset &);
	void SetHeightAndBalance (fstream *, offset &);
	void Write (fstream *) const;		// update node to disk

	ostream & Write_AllTNodes (ostream &, fstream *) const;
};

/* 
 * Function: Set_Debug_On
 * Purpose: Sets value of debug to true
 */
template <class Whatever>
void Tree<Whatever> :: Set_Debug_On (void) {
	debug_on = TRUE;
}

/*
 * Function: Set_Debug_Off
 * Purpose: Sets value of debug to false
 */
template <class Whatever>
void Tree<Whatever> :: Set_Debug_Off (void) {
	debug_on = FALSE;
}

/*
 * Function:	Tree's Insert
 * Description:	Inserts element into binary tree. Inserts at root if tree is 
 *				empty, otherwise delegates to TNode's Insert
 * Parameters:	element - expected to be data stored in the TNode, they must be 
 *				same type as rest of objects present in tree
 * Return:		returns true for successful insert, false otherwise
 */

template <class Whatever>
unsigned long Tree<Whatever> :: Insert (Whatever & element) {
	
	if (!fio)
		cerr<<"fio is corrupt in Tree's Insert";
	
	// increments operation
	IncrementOperation();
	
	// seek to beginning of file
	fio->seekg(0, ios::end);
	
	// put ending in file
	offset ending = fio->tellg();
	
	// insert first root node into tree
	if (root == ending)
		TNode<Whatever>rootTNode(element,fio,occupancy);
	
	//  calls TNode's Insert to insert all non-root nodes into tree
	else {
		TNode<Whatever>readRootNode(root,fio);
		readRootNode.Insert(element,fio,occupancy,root);
	}

	return TRUE;
}

/*
 * Function:	TNode's ReplaceAndRemoveMin
 * Description:	Called when removing a TNode with 2 children, replaces that
 *				Tnode with the minimum TNode in its right subtree to
 *				maintain tree structure
 * Parameters:	targetTNode - reference to TNode to rmeove that has 2 children
 *				fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 *				PositionInParent - reference to TNode position in parent
 *				TNode used to get current TNode's offset in datafile
 * Return:		nothing
 */

template <class Whatever>
void TNode<Whatever> :: ReplaceAndRemoveMin (TNode<Whatever> & targetTNode, 
	fstream * fio, offset & PositionInParent) {
	
	// If there is node to left
	if (left){
		
		// Traverses to left recursively  
		TNode<Whatever> toBeReplaced(left,fio);
		toBeReplaced.ReplaceAndRemoveMin(targetTNode,fio,left);
		
		// Set height and balance during recursion
		SetHeightAndBalance(fio,PositionInParent);
	}

	// Overrides node with 2 children with smallest node in right
	// subtree, sets PositionInParent
	else {
		targetTNode.data = data;
		PositionInParent = right;
	}
}

/* Function:	TNode's Remove
 * Description:	Removes matching data from binary tree. Called from Tree's
 *				Remove, SetHeightAndBalance, and TNode's Remove recursively.
 * Parameters:	elementTnode - reference to TNode containing data that
 *				identifies element to remove
 *				fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 *				occupancy - reference to occupancy of tree
 *				PositionInParent - reference to TNode position in parent
 *				used to get to current TNode's offset in datafile
 *				fromSHB - true/false, keeps track of whether Remove was
 *				called from SHAB so that Remove can determine whether or not
 *				to call SHAB
 * Return:		returns true for succesful removal, false otherwise
 */

template <class Whatever>
unsigned long TNode<Whatever> :: Remove (TNode<Whatever> & elementTNode,
	fstream * fio, long & occupancy, offset & PositionInParent,
	long fromSHB) {
	
	// store return value of Remove after recursive call
	long removed;
	
	// if element to remove if found
	if (elementTNode.data == data){

		// change data in elementTNode
		elementTNode.data = data;
		
		// if node being removed is leaf node, set PositionInParent
		// to null
		if (!left && !right)
			PositionInParent = NULL;

		// if node being removed has 2 children
		else if (left && right){

			//replace node with 2 children with smallest node in
			//right subtree
			TNode<Whatever>toReplace(right,fio);
			toReplace.ReplaceAndRemoveMin(*this,fio,right);
			
			// if Remove wasn't called from SHAB, set height/balance
			if (fromSHB == FALSE)
				SetHeightAndBalance(fio,PositionInParent);
		
			else
				Write(fio);
		}
		
		// if node being rmeoved has 1 child
		else {
			// if there is a right child, set PIP to existing child
			if (!right)
				PositionInParent = left;
			// if there is a left child, set PIP to existing child
			else
				PositionInParent = right;
		}	
		// remove succesful
		return TRUE;
	}
	
	// if node to remove is less than current node
	else if (elementTNode.data < data){
		
		// if no left node, return false
		if (!left)
			return FALSE;

		// call Remove to keep looking for node to remove
		else {
			TNode<Whatever>toRemove(left,fio);
			removed = toRemove.Remove(elementTNode,fio,occupancy,left,FALSE);
			
			// if Remove not caled from SHAB, set height and balance
			if(fromSHB == FALSE)
				SetHeightAndBalance(fio,PositionInParent);
			
			// return result of recursive Remove call (true/false)
			return removed;
		}
	}
	
	// if node to remove is more than current node
	else if (elementTNode.data > data){
		
		// if no right node, return false
		if (!right)
			return FALSE;
		
		// cal Remove to keep looking for node to remove
		else {
			TNode<Whatever>toRemove(right,fio);
			removed = toRemove.Remove(elementTNode,fio,occupancy,right,FALSE);
			
			// if Remove not called from SHAB, set height/balance
			if (fromSHB == FALSE)
				SetHeightAndBalance(fio,PositionInParent);

			// return result of recursive Remove call (true/false)
			return removed;
		}
	}
	
	// return false for unsuccessful removeal
	return FALSE;

}
	
/*
 * Function:	Tree's Remove
 * Description:	Removes element from tree. Delegates to TNode's Remove when
 *				tree is not empty.
 * Parameters:	element - data stored in TNode, must be same type as rest of
 *				objects in tree
 * Return:		returns true for succesful removal, false otherwise
 */

template <class Whatever>
unsigned long Tree<Whatever> :: Remove (Whatever & element) {
	
	// if removal successful or not
	long removed;
	
	// increment operation count
	IncrementOperation();
	
	// if tree is not empty
	if (occupancy > 0){

		// create Tnode with element and call TNode's Remove 
		TNode<Whatever> elementTNode(element);
		TNode<Whatever> toRemove(root,fio);

		// save return value of previous call to TNode's Remove
		removed = toRemove.Remove(elementTNode,fio,occupancy,root,FALSE);

		// save data in node
		element = elementTNode.data;
		
		// to remove root node with no children, decrement occupancy
		// and reset root
		if (occupancy == 1){
			occupancy--;
			ResetRoot();
			return TRUE;
		}

		// return false for unsuccessful remove
		if (!removed)
			return FALSE;
		
		// return true for succesfull remove, decrement occupancy 
		else if (removed){
			occupancy--;
			return TRUE;
		}
	}
	
	// return false for unsuccessful remove
	return FALSE;
}

/* Function:	TNode's SetHeightAndBalance
 * Description:	Updates height and balance of current TNode. Called from
 *				TNode's Remove, TNode's Insert, and RARM
 * Paramter:	fio - filestrem corresponding to datafile where tree is
 *				stored on disk
 *				PositonInParent - reference to TNode position in parent
 *				TNode use to get to current TNode's offset in datafile
 * Return:		none
 */

template <class Whatever>
void TNode<Whatever> :: SetHeightAndBalance (fstream * fio,
	offset & PositionInParent) {

	// intialise left height, right height as if doesn't exist 
	int rHeight = -1;
	int lHeight = -1;
	long occupancy = 0;

	// if there is a right node, update its height
	if (left){
		TNode<Whatever>lNode(left,fio);
		lHeight = lNode.height;
	}

	// if there is a left node, update its height
	if (right){
		TNode<Whatever>rNode(right,fio);
		rHeight = rNode.height;
	}

	// if left height is less than right height, update height of
	// current node to one more than height of right node
	if (lHeight > rHeight)
		height = lHeight + 1;
	
	// if left height is greater than right height, update height of
	// current node to one more than height of left node
	else
		height = rHeight + 1;
	
	// set balance according to folowing expression
	balance = lHeight - rHeight;

	// if balance is more than threshold value
	if (abs(balance) > THRESHOLD ){

		// Remove node resulting in imbalance
		TNode<Whatever>nodeData(data);
		Remove(nodeData,fio,occupancy,PositionInParent,TRUE);

		// Insert data back into node
		TNode<Whatever>replacementTNode(PositionInParent,fio);
		replacementTNode.Insert(nodeData.data,fio,occupancy,PositionInParent);
	}
	else
		Write(fio);
}

/* Function:	Tree's GetCost
 * Description:	Returns value of Tree<Whatever>::cost variable
 * Parameters:	none
 * Return:		see description
 */

template <class Whatever>
long Tree <Whatever> :: GetCost () {
	return cost;
}

/* Function:	Tree's GetOperation
 * Description:	Returns value of Tree<Whatever>::operation variable
 * Paramters:	none
 * Return:		see description
 */

template <class Whatever>
long Tree <Whatever> :: GetOperation () {
	return operation; // return operation;
}

/* Function:	Tree's IncrementCost
 * Description:	Increments value of Tree<Whatever>::cost variable. Called
 *				when a read or write to disk occurs
 * Paramters:	none
 * Return:		none
 */

template <class Whatever>
void Tree <Whatever> :: IncrementCost () {
	cost++;
}

/* Function:	Tree's IncrementOperation
 * Descriptoin:	Increments value of Tree<Whatever>::operation varaible.
 *				Called when a Tree op occurs (insert, lookup, remove)
 * Paramter:	none
 * Return:		none
 */

template <class Whatever>
void Tree <Whatever> :: IncrementOperation () {
	operation++;
}

/* Function:	Tree's ResetRoot()
 * Descriptoin:	Resets the root datafield of this tree to be at the end of
 *				the datafile. This should be called when the last TNode has
 *				been removed from the tree
 * Parameters:	none
 * Return:		none
 */

template <class Whatever>
void Tree <Whatever> :: ResetRoot () {
	
	// seek to end of file
	fio->seekp(0,ios::end);

	// set root to point to end of file
	root = fio->tellp();

}

/* Function:	TNode's Insert
 * Description:	Inserts an element into the binary tree. Called from Tree's
 *				Insert, SHAB, and TNode's Insert (recursively)
 * Paramters:	element - data stored in TNode
 *				fio - filestream corresponding to datafile where Tree is
 *				stored in disk
 *				occupancy - reference to occupancy of tree
 *				PositionInParent - reference to TNode position in parent
 *				TNode used to get to current TNode's offset in datafile
 * Return:		returns true for successful insert, false otherwise
 */

template <class Whatever>
unsigned long TNode<Whatever> :: Insert (Whatever & element, fstream * fio,
	long & occupancy, offset & PositionInParent) {
	
	// duplicate insertion case
	if (element == data){

		// replace data in current node with data in node to
		// re-insert
		data = element;
		Write(fio);
	}
	
	// if node to be inserted is less than current node
	else if (element < data){

		// if no left node
		if (!left){

			// Insert new node here
			TNode<Whatever>readRootNode(element,fio,occupancy);
			left = readRootNode.this_position;
		}
		
		// if there is a left node already, keep going left, calling
		// Insert recursively
		else {
			TNode<Whatever>readRootNode(left, fio);
			readRootNode.Insert(element,fio,occupancy,left);
		}
		
		// update height and balance of tree
		SetHeightAndBalance(fio,PositionInParent);
	}
	
	// if node to be inserted is larger than current node
	else {

		// if no right node 
		if(!right){

			// Insert new node here
			TNode<Whatever>readRootNode(element,fio,occupancy);
			right = readRootNode.this_position;
		}

		// if there is a right node already, keep going right,
		// calling Insert recursively
		else {
			TNode<Whatever>readRootNode(right, fio);
			readRootNode.Insert(element,fio,occupancy,right);
		}
		
		// update height and balance of tree
		SetHeightAndBalance(fio,PositionInParent);
	}

	return TRUE;
}

/* optional recursive TNode Lookup function */

/* Function:	TNode's Lookup
 * Description:	Searches for element in tree recursively
 * Parameter:	element - data stored in TNode
 *				fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 * Return:		returns true for successful lookup, false otherwise
 */

template <class Whatever>
unsigned long TNode<Whatever> :: Lookup(Whatever & element, fstream *fio) const{

	// if element looking for matches data in current node
	if (element == data){

		// set element equal to data in current node
		element = data;

		// return true for successul lookup
		return TRUE;
	}

	// if element looking for is less than data in current node
	else if (element < data){

		// if no left node
		if (!left)

			// return false for unsuccessful lookup
			return FALSE;

		// if there is a left node
		else {

			// return result of Recursive call to Lookup on left
			// node
			TNode<Whatever>leftTNode(left,fio);
			return leftTNode.Lookup(element,fio);
		}
	}

	// if element looking for is greater than data in current node
	else {

		// if not right node
		if (!right)

			// return false for unsuccessful lookup
			return FALSE;
		
		// if there is a right node
		else {

			// return result of Recursive call to Lookup on right
			// node
			TNode<Whatever> rightTNode(right,fio);
			return rightTNode.Lookup(element,fio);
		}
	}
}

/* Function:	Tree's Lookup
 * Description:	Searches for element in tree, loop-based. Call's TNode's
 *				Lookup
 * Paramter:	element - data stored in TNode
 * Return:		returns true for successful lookup, false otherwise
 */

template <class Whatever>
unsigned long Tree<Whatever> :: Lookup (Whatever & element) const {
	IncrementOperation();
	
	// if tree is empty, return false for unsuccessful lookup
	if (occupancy == 0)
		return FALSE;

	// if tree is not empty, call TNode's Lookup, and return result
	// of that
	else {
		TNode<Whatever> rootTNode(root,fio);
		return rootTNode.Lookup(element,fio);
	}
}

/* Function:	TNode's Read 
 * Descriptoin:	Reads a TNode which is present on datafile into memory.
 *				TNode is read from position. Tnode's info in datafile
 *				overides this TNode's data
 * Paramater:	position - offset in datafile corresponding to position of
 *				TNode we wish to read into memory
 *				fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 * Return:		none
 */

template <class Whatever>
void TNode<Whatever> :: Read (const offset & position, fstream * fio) {
	
	// seek to a position
	fio->seekg(position);
	// Read tnode 
	fio->read((char*)this, sizeof(TNode<Whatever>));
	
	// debug msg
	if(Tree<Whatever>::debug_on)
		cerr<<COST_READ<<(char*)this<<"]\n";
	
	// increment cost every time tnode is read
	Tree<Whatever>::IncrementCost();
	
}

/* Function:	TNode's Read Constructor
 * Descriptoin:	TNnode is created in memory to read an existing TNode on
 *				disk into memory. Used when you want to traverse tree.
 *				Initialises all fields of TNode in memory from a TNode on
 *				disk, achieved via calling TNode's Read function.
 * Paramters:	position - reference to location within file where TNode
 *				exists (left, right, roo)
 *				fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 * Return:		TNode
 */

template <class Whatever>
TNode<Whatever> :: TNode (const offset & position, fstream * fio) {

	// calls TNode's Read function
	Read(position, fio);
}

/* Function:	TNode's Write Constructor
 * Description:	TNode is created i memory to write a new TNode to disk. Used
 *				to insert new object into tree. Initialises all fields of
 *				TNode in memory, then writes itself to disk.
 * Parameters:	element - reference to data stored in TNode
 *				fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 *				occupancy - refernce to tree's occupancy
 * Return:		TNode
 */

template <class Whatever>
TNode<Whatever> :: TNode (Whatever & element, fstream * fio, long & occupancy): 
			data (element), height (0), balance (0), left (0), 
			right (0) {

	// seek to end of file
	fio->seekp(0, ios::end);

	// set this_position to position put in file
	this_position = fio->tellp();
	Write(fio);

	// increment occupancy 
	occupancy++;
	
}

/* Function:	TNode's Write
 * Description:	Writes this TNode object to disk at this_position in datfile
 * Parameter:	fio - filestream corresponding to datafile where Tree is
 *				stored on disk
 * Return:		none
 */

template <class Whatever>
void TNode<Whatever> :: Write (fstream * fio) const {
	
	// increment cost every time tnode is written to disk
	Tree<Whatever>::IncrementCost();

	// seek to this_position
	fio->seekp(this_position);

	// write "this" into file
	fio->write((const char*)this, sizeof(TNode<Whatever>));
	
	// debug msg
	if (Tree<Whatever>::debug_on)
		cerr<<COST_WRITE<<(const char*)this<<"]\n";
	
}

/* Function:	Tree Constructor
 * Description:	Allocates tree object. Checks datafile to see if it contains
 *				Tree data. if it is empty, root and occupancy fields are
 *				written to the file. If there is data in datafile, root and
 *				occupancy fields are read into memory
 * Parameter:	fio - filestream corresponding to datafile where tree is to
 *				be stored on disk
 * Return:		Tree
 */

template <class Whatever>
Tree<Whatever> :: Tree (const char * datafile) :
	fio (new fstream (datafile, ios :: out | ios :: in)) {
	
	// intialises variables for empty tree
	occupancy = 0;
	root = 0;
	static long treeCount;
	
	// sets tree_count increment treeCount
	tree_count = ++treeCount;
	
	// debug msg
	if (Tree<Whatever>::debug_on)
		cerr<<TREE<<tree_count<<ALLOCATE;

	// seek to beginning of file
	fio->seekp(0, ios::beg);

	// put beginning in file 
	offset beginning = fio->tellp();
	
	// seek to end of file
	fio->seekp(0, ios::end);

	// put ending in file
	offset ending = fio->tellp();
	
	// if datafile is empty
	if (beginning == ending){

		// seek to beginning of file
		fio->seekp(0, ios::beg);

		// write root and occupancy fields to file
		fio->write((const char*)&root, sizeof(root));
		fio->write((const char*)&occupancy, sizeof(occupancy));
		
		// set root to point to beginning of file
		root = fio->tellp();
	}

	// if datafile not empty
	else {

		// seek to beginning of file
		fio->seekg(0, ios::beg);

		// read root and occupancy fields into memory
		fio->read((char*)&root, sizeof(root));
		fio->read((char*)&occupancy, sizeof(occupancy));
		
		// set root to point to beginning of file
		root = fio->tellg();
	}
}

template <class Whatever>
Tree<Whatever> :: ~Tree (void)
/***************************************************************************
% Routine Name : Tree :: ~Tree  (public)
% File :         Tree.c
% 
% Description :  deallocates memory associated with the Tree.  It
%                will also delete all the memory of the elements within
%                the table.
***************************************************************************/

{
	// debug msg
	if (Tree<Whatever>::debug_on)
		cerr<<TREE<<tree_count<<DEALLOCATE<<"\n";
	// seek to beginning of file
	fio->seekp(0, ios::beg);

	// write root and occupancy fields into memory
	fio->write((const char*)&root, sizeof(root));
	fio->write((const char*)&occupancy, sizeof(occupancy));
	
	// deallocate fstream object, calls fstream destuctor
	delete fio;
}	/* end: ~Tree */

template <class Whatever>
ostream & operator << (ostream & stream, const TNode<Whatever> & nnn) {
	stream << "at height:  :" << nnn.height << " with balance:  "
		<< nnn.balance << "  ";
	return stream << nnn.data << "\n";
}

template <class Whatever>
ostream & Tree<Whatever> :: Write (ostream & stream) const
/***************************************************************************
% Routine Name : Tree :: Write (public)
% File :         Tree.c
% 
% Description : This funtion will output the contents of the Tree table
%               to the stream specificed by the caller.  The stream could be
%               cerr, cout, or any other valid stream.
%
% Parameters descriptions :
% 
% name               description
% ------------------ ------------------------------------------------------
% stream             A reference to the output stream.
% <return>           A reference to the output stream.
***************************************************************************/

{
        long old_cost = cost;

	stream << "Tree " << tree_count << ":\n"
		<< "occupancy is " << occupancy << " elements.\n";

	fio->seekg (0, ios :: end);
	offset end = fio->tellg ();

	// check for new file
	if (root != end) {
		TNode<Whatever> readRootNode (root, fio);
		readRootNode.Write_AllTNodes (stream, fio);
	}

        // ignore cost when displaying nodes to users
        cost = old_cost;

	return stream;
}

template <class Whatever>
ostream & TNode<Whatever> ::
Write_AllTNodes (ostream & stream, fstream * fio) const {
	if (left) {
		TNode<Whatever> readLeftNode (left, fio);
		readLeftNode.Write_AllTNodes (stream, fio);
	}
	stream << *this;
	if (right) {
		TNode<Whatever> readRightNode (right, fio);
		readRightNode.Write_AllTNodes (stream, fio);
	}

	return stream;
}

