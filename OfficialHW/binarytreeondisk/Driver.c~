/******************
 *
 * Megan Chang
 * CSE 12 Fall 2015
 * cs12xee
 * Dec 3 2015
 * Assignment Nine
 *
 * File: Driver.c
 * Description: Contains main method, used to execute Tree.c.
 *
 *****************/

#include <iostream>
#include <cstdio>
#include <string>
#include <getopt.h>
#include "Driver.h"
#include "SymTab.h"
#include <fstream>

using namespace std;

#ifdef NULL
#undef NULL
#define NULL 0
#endif

ostream & operator << (ostream & stream, const UCSDStudent & stu) {
	return stream << "name:  " << stu.name
		<< " with studentnum:  " << stu.studentnum;
}

int main (int argc, char * const * argv) {
	char buffer[BUFSIZ];
	char command;
	long number;
	char option;
	
	// declare (i/o)stream*, assign it to &cin/&cout by default
	istream* IS = &cin;
	ostream* OS = &cout;

	// variable to determine if terminate
	long terminate = 0;

	SymTab<UCSDStudent>::Set_Debug_Off ();

	while ((option = getopt (argc, argv, "x")) != EOF) {

		switch (option) {
			case 'x': SymTab<UCSDStudent>::Set_Debug_On ();
					  break;
		}       
	}
	
	// pass in Driver.datafile as parameter
	SymTab<UCSDStudent> ST("Driver.datafile");
	ST.Write (cout << "Initial Symbol Table:\n" );
	
	while (cin) {
		command = NULL;         // reset command each time in loop
		*OS << "Please enter a command ((f)ile,(i)nsert, "
			<< "(l)ookup, (r)emove, (w)rite):  ";
		*IS >> command;

		switch (command) {
			
			// Driver command for file input
			case 'f': {
						// print prompt to terminal
						*OS << "Please enter file name for commands: ";

						// read input from terminal into buffer
						*IS >> buffer;

						// start reading from file for input
						IS = new ifstream(buffer);
						OS = new ofstream("/dev/null");
						
						// file input terminate set to true
						terminate = 1;
						
						break;
			}	

			case 'i': {
						  *OS << "Please enter UCSD student name to insert:  ";
						  *IS >> buffer;  // formatted input

						  *OS << "Please enter UCSD student number:  ";
						  *IS >> number;

						  UCSDStudent stu (buffer, number);

						  // create student and place in symbol table
						  ST.Insert (stu);
						  break;
					  }
			case 'l': { 
						  unsigned long found;    // whether found or not

						  *OS << "Please enter UCSD student name to lookup:  ";
						  *IS >> buffer;  // formatted input

						  UCSDStudent stu (buffer, 0);
						  found = ST.Lookup (stu);

						  if (found)
							  cout << "Student found!!!\n" << stu << "\n";
						  else
							  cout << "student " << buffer << " not there!\n";
						  break;
					  }
			case 'r': { 
						  unsigned long removed;

						  *OS << "Please enter UCSD student name to remove:  ";
						  *IS >> buffer;  // formatted input

						  UCSDStudent stu (buffer, 0);
						  removed = ST.Remove(stu);

						  if (removed)
							  cout << "Student removed!!!\n" << stu << "\n";
						  else
							  cout << "student " << buffer << " not there!\n";
						  break;
					  }
			case 'w':
					  ST.Write (cout << "The Symbol Table contains:\n");
		}
		
		// part of switch case (f)
		// if terminate is true or end of command input file
		if (terminate && !command){

			// delete istream and ostream pointers
			delete IS;
			delete OS;

			// reassing pointers to &cin and &cout
			IS = &cin;
			OS = &cout;

			// set terminate to false
			terminate = 0;
		}
	}

	ST.Write (cout << "\nFinal Symbol Table:\n");

	if (ST.GetOperation() != 0) {
		cout << "\nCost of operations: ";
		cout << ST.GetCost();
		cout << " tree accesses";

		cout << "\nNumber of operations: ";
		cout << ST.GetOperation();

		cout << "\nAverage cost: ";
		cout << (((float)(ST.GetCost()))/(ST.GetOperation()));
		cout << " tree accesses/operation\n";
	}

	else
		cout << "\nNo cost information available.\n";
}
